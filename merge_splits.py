from argparse import ArgumentParser
from math import sqrt
from splits_util import *

def normalize_splits(sma,smb,norm_split_count=False):
    sma_ = dict()
    smb_ = dict()
    #print(norm_split_count)
    tota = sum(sma.values()) / (len(sma) if norm_split_count else 1)
    totb = sum(smb.values()) / (len(smb) if norm_split_count else 1)
    mx = max(max(smb.values()),max(sma.values()))
    #always scale b w.r.t. a
    ascale = 1.0
    bscale = tota/totb
    for k,v in sma.items():
        sma_[k] = ascale * v/mx
    for k, v in smb.items():
        smb_[k] = bscale * v/mx
    return sma_,smb_

def merge_splits(splitsa,splitsb,merge_function=arith_mean,default=0,norm_split_count=False):
    sma = split_map(splitsa)
    smb = split_map(splitsb)
    sma, smb = normalize_splits(sma,smb,norm_split_count=norm_split_count)
    total = {}
    keyz = set(sma.keys())
    keyz.update(smb.keys())
    for k in keyz:
        wa = sma.get(k,default)
        wb = smb.get(k,default)
        total[k]=merge_function(wa,wb)
    return total 


def split_map(splits):
    sm = {}
    for w,s in splits:
        sm[tuple(s)] = w
    return sm

def write_splits(sm):
    for taxa,v in sm.items():
        print(v,end='\t')
        print('\t'.join(list(taxa)))


MF_MAP = {'arithmetic':arith_mean, 'geometric':geom_mean,'ratio':ratio}

def main():
    parser = ArgumentParser()
    parser.add_argument('sfa')
    parser.add_argument('sfb')
    parser.add_argument('--mergefun',choices=list(MF_MAP.keys()),default='geometric')
    parser.add_argument('--norm-split-count',action='store_true')
    args = parser.parse_args()
    splitsa = read_sans_splitfile(args.sfa)
    splitsb = read_sans_splitfile(args.sfb)
    tot = merge_splits(splitsa,splitsb,merge_function=MF_MAP[args.mergefun],norm_split_count=args.norm_split_count)
    write_splits(tot)

main()