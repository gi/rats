from argparse import ArgumentParser
from math import ceil
from splits_util import read_sans_splitfile, canonize_splits

class ColorGradient:
    def __init__(self,low=0.0,high=1.0,colora=(140/255,61/255,157/255),colorb=(159/255,43/255,149/255),grey_factor=1):
        self.low =low
        self.high= high
        self.colora=colora
        self.colorb=colorb
        self.grey_factor=grey_factor
    def get_rgb(self,v):
        ratio = v/(self.high-self.low)
        aratio=1-ratio
        mycolor = self.colora if ratio > 0.5 else self.colorb
        boringess = 1-abs(ratio-0.5)/0.5
        boringess = boringess**6
        scaledown = 1- boringess*self.grey_factor
        mycolor = (scaledown*mycolor[0],scaledown*mycolor[1],scaledown*mycolor[2])
        return mycolor

def to_255(color):
    return (ceil(255*color[0]),ceil(255*color[1]),ceil(255*color[2]))


def is_nexus_skipline(l):
    s = l.strip()
    return s in ['',';']


class NexEdge:
    def __init__(self,l):
        self.id = l[0]
        self.frm = l[1]
        self.to = l[2]
        self.data = {}
        for x in l[3::]:
            if not "=" in x:
                print("Warning optional attribute with no equals in it will be ignored.")
                continue
            xs = x.split("=")
            self.data[xs[0]] = xs[1]
    def to_nx_line(self):
        return "{} {} {} {},\n".format(self.id,self.frm,self.to,' '.join(["{}={}".format(x,y) for x,y in self.data.items()]))

class NexSplit:
    def __init__(self,id,w,tx):
        self.id = id
        self.w = w
        self.tx =tx

def edges_by_splits(edges):
    d = {}
    for e in edges:
        s = e.data['s']
        if not s in d:
            d[s] = []
        d[s].append(e)
    return d


def splits_by_taxa(nexsplits,translator):
    spls = []
    universe_ = set()
    for s in nexsplits:
        #print(s.tx)
        splittaxa = [translator[y] for y in s.tx]
        universe_.update(set(splittaxa))
        spls.append((s.id,splittaxa))
    #print(universe_)
    spls = canonize_splits(spls,universe_)
    d = {}
    for i,tx in spls:
        idstr = tuple(tx)
        d[idstr] = i
    return d



def parse_nexus_bad(nexusfile):
    txmap = {}
    edges = []
    splits = []
    with open(nexusfile) as f:
        reading = {}
        reading['taxa'] =False
        reading['splits'] = False
        reading['edges'] = False
        for line in f:
            if line.strip() == 'EDGES':
                reading['edges'] = True
                continue
            if line.strip() == "END; [Network]":
                reading['edges'] = False
                continue
            if line.strip() == "BEGIN Taxa;":
                reading['taxa']=True
                continue
            if line.strip() == "END; [Taxa]":
                reading['taxa'] = False
                continue
            if line.strip() == "BEGIN Splits;":
                reading['splits'] = True
                continue
            if line.strip() == "END; [Splits]":
                reading['splits'] = False
                continue
            if is_nexus_skipline(line):
                continue
            if reading['edges']:
                entries = line.strip().strip(",").split(' ')
                edges.append(NexEdge(entries))
            elif reading['taxa']:
                if not line.startswith('['):
                    continue
                interm =  line.strip()[1::].split("]")
                tid = interm[0]
                tname = interm[1].split("'")[1]
                txmap[tid] = tname
            elif reading['splits']:
                if not line.startswith('['):
                    continue
                interm =  line.strip()[1::].split("]")
                sid = interm[0].split(',')[0]
                interm2 = interm[1].strip().strip(',').split()
                w = interm2[0]
                tx = interm2[1::]
                splits.append(NexSplit(sid,w,tx))
    return txmap,edges,splits

def to_string(cl):
    a,b,c = to_255(cl)
    return str(a),str(b),str(c)
    

def recolor_edges(txmap,edges,nx_splits,ratio_splits):
    spl_by_tx = splits_by_taxa(nx_splits,txmap)
    #print(spl_by_tx)
    e_by_spl = edges_by_splits(edges)
    gradient = ColorGradient()
    for r,spltx in ratio_splits:
        color = gradient.get_rgb(r)
        nx_split_id = spl_by_tx[tuple(spltx)]
        es = e_by_spl[nx_split_id]
        for e in es:
            e.data['fg']=' '.join(to_string(color))
            e.data['l']='3'
    return e_by_spl

def replace_edges(nx_lines,edges):
    edges = sorted(edges,key=lambda x: int(x.id))
    nx_lines_new = []
    edges_start = False
    edges_end = False
    for line in nx_lines:
        if line.strip()=='EDGES':
            nx_lines_new.append(line)
            edges_start = True
            continue
        if line.strip()==';' and edges_start:
            edges_end = True
        if edges_start and edges_end:
            #insert the new edges
            for e in edges:
                nx_lines_new.append(e.to_nx_line())
            edges_start = False
            edges_end=False
        if not edges_start:
            nx_lines_new.append(line)
    return nx_lines_new


def main():
    parser = ArgumentParser()
    parser.add_argument('nexus')
    parser.add_argument('ratiofile')
    args = parser.parse_args()
    ratio_splits = read_sans_splitfile(args.ratiofile)
    txmap,edges,splits = parse_nexus_bad(args.nexus)
    #print(txmap,edges,splits)
    emap = recolor_edges(txmap,edges,splits,ratio_splits)
    newedges=[]
    for es in emap.values():
        newedges.extend(es)
    with open(args.nexus) as f:
        for l in replace_edges(f.readlines(),newedges):
            print(l,end='')
main()