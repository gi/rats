#include <iostream>
#include <fstream>
#include <map>
#include <chrono>
using namespace std;

enum class type {
    NONE,
    INPUT_FILE,   // list of sequence files, one per line
    GRAPH_FILE,   // Bifrost graph, .gfa.gz + .color.bfg
    BREAK_FILE,   // list of left/right breakpoint k-mers
    MARKER_FILE,  // list of all unitig marker sequences
    UNIMOG_FILE,  // genomes spelled with unitig markers
    SPLITS_FILE,  // list of splits, sorted by weight
};

enum class mode {
    NONE,
    READ,
    WRITE,
};

/**
 * This class provides an interface for handling different file types.
 */
class file {
 private:
    string _path;
    type   _type;
    mode   _mode;

 protected:
    map<string, string> properties;

 public:
    file() { _path.clear(); _type = type::NONE; _mode = mode::NONE; properties.clear(); }
   ~file() { _path.clear(); _type = type::NONE; _mode = mode::NONE; properties.clear(); }

    file(const string& path, const type& type, const mode& mode);

    operator  string()                  { return _path; }
    inline    bool empty()              { return _path.empty(); }
    constexpr bool is(const type& type) { return _type == type; }
    constexpr bool is(const mode& mode) { return _mode == mode; }

    inline string&  operator[](const string& key)             { return properties[key]; }
    friend ostream& operator<<(ostream& os, const file& file) { return os << file._path; }
};

/**
 * This class contains some helpful utility functions.
 */
class util {

 public:

    /**
     * This function converts a command line argument to a string.
     *
     * @param argc argument count
     * @param argv argument vector
     * @param i argument index
     * @return string
     */
    static string atos(int& argc, char* argv[], int& i);

    /**
     * This function converts a command line argument to a number.
     *
     * @param argc argument count
     * @param argv argument vector
     * @param i argument index
     * @return number
     */
    static uint64_t aton(int& argc, char* argv[], int& i);

    /**
     * This function converts a string argument to a number.
     *
     * @param param parameter
     * @param args argument
     * @return number
     */
    static uint64_t ston(const string& param, const string& args);

    /**
     * This function displays a duration in a human readable format.
     *
     * @param time duration
     * @return formatted string
     */
    static string format_time(const chrono::high_resolution_clock::duration& time);

};
