#include "main.h"
#include "../ansi.h"

/**
 * This is the entry point of the program.
 *
 * @param argc number of cmd args
 * @param argv cmd args
 * @return exit status
 */
int main(int argc, char* argv[]) {
    ios_base::sync_with_stdio(false);

    // check for a new version of RATS Speller at program start
    if (!system("wget --timeout=1 --tries=1 -qO- https://gitlab.ub.uni-bielefeld.de/gi/rats/raw/main/src/speller/main.h | grep -q RATS_VERSION")
      && system("wget --timeout=1 --tries=1 -qO- https://gitlab.ub.uni-bielefeld.de/gi/rats/raw/main/src/speller/main.h | grep -q " RATS_VERSION)) {
        $link << "NEW VERSION AVAILABLE: https://gitlab.ub.uni-bielefeld.de/gi/rats" << _end$;
    }
    // print a help message describing the program arguments
    if (argc <= 1 || strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0) {
        $out << end$;
        $out << "RATS Speller | version " << RATS_VERSION << end$;
        $out << "Usage: Speller [PARAMETERS]" << end$;
        $out << end$;
        if (argc <= 1) help::print_usage();
        else           help::print_extended_usage();
        $out << end$$;
    }

    file input;    // Input FASTA files: list of sequence files, one per line
    file graph;    // Input Graph files: load Bifrost graph, file name prefix
    file brake;    // Input Break files: list of left/right breakpoint k-mers
    file output;   // Output Marker file: list of all unitig marker sequences
    file unimog;   // Output UniMoG file: genomes spelled with unitig markers

   #ifndef DEBUG
     uint64_t T = thread::hardware_concurrency();    // Number of hardware-threads
   #else
     uint64_t T = 1;    // Use only one thread when debugging (for proper logging)
   #endif

    bool verbose = false;    // Print some information messages during execution
    bool VERBOSE = false;    // Print more information messages during execution

    // parse the command line arguments and update the variables above
    for (int i = 1; i < argc; ++i) {
        if (strcmp(argv[i], "-i") == 0 || strcmp(argv[i], "--input") == 0)
            input = file(util::atos(argc, argv, ++i), type::INPUT_FILE, mode::READ);
        else if (strcmp(argv[i], "-g") == 0 || strcmp(argv[i], "--graph") == 0)
            graph = file(util::atos(argc, argv, ++i), type::GRAPH_FILE, mode::READ);
        else if (strcmp(argv[i], "-b") == 0 || strcmp(argv[i], "--break") == 0)
            brake = file(util::atos(argc, argv, ++i), type::BREAK_FILE, mode::READ);
        else if (strcmp(argv[i], "-o") == 0 || strcmp(argv[i], "--output") == 0)
            output = file(util::atos(argc, argv, ++i), type::MARKER_FILE, mode::WRITE);
        else if (strcmp(argv[i], "-u") == 0 || strcmp(argv[i], "--unimog") == 0)
            unimog = file(util::atos(argc, argv, ++i), type::UNIMOG_FILE, mode::WRITE);

        else if (strcmp(argv[i], "-k") == 0 || strcmp(argv[i], "--kmer") == 0)
            speller::kmer = util::aton(argc, argv, ++i);    // Length of k-mers (default: auto)

        else if (strcmp(argv[i], "-c") == 0 || strcmp(argv[i], "--color") == 0)
            speller::color = util::aton(argc, argv, ++i);    // Discard single unitigs with less than [n] colors
        else if (strcmp(argv[i], "-C") == 0)    /* hidden option */
            speller::COLOR = util::aton(argc, argv, ++i);    // Discard core components with less than [n] colors
        else if (strcmp(argv[i], "-l") == 0 || strcmp(argv[i], "--length") == 0)
            speller::length = util::aton(argc, argv, ++i);    // Discard single unitigs shorter than [n] bases
        else if (strcmp(argv[i], "-L") == 0)    /* hidden option */
            speller::LENGTH = util::aton(argc, argv, ++i);    // Discard core components shorter than [n] bases

        else if (strcmp(argv[i], "-s") == 0 || strcmp(argv[i], "--soft") == 0)
            speller::soft = true;    // Keep all (-l/c)-filtered-out unitigs with a marker '0'
        else if (strcmp(argv[i], "-S") == 0)    /* hidden option */
            speller::soft = true,    // Keep all (-l/c)-filtered-out unitigs with a marker '0'
            speller::SOFT = true;    // Keep also skipped (not found) k-mers with a marker '0'

        else if (strcmp(argv[i], "-m") == 0 || strcmp(argv[i], "--merge") == 0)
         // speller::merge = util::aton(argc, argv, ++i);    // Merge bubbling unitigs shorter than [n] bases
            $err << "Error: option -m is not implemented yet, did you mean -M?" << _end$$;
        else if (strcmp(argv[i], "-M") == 0)    /* hidden option */
            speller::MERGE = true;    // Represent core connected components by the same marker
        else if (strcmp(argv[i], "-r") == 0 || strcmp(argv[i], "--range") == 0)
            speller::range = true;    // Collapse consecutive runs of the same marker in UniMoG
        else if (strcmp(argv[i], "-R") == 0)    /* hidden option */
            speller::RANGE = true;    // Collapse alternating runs of the same marker in UniMoG

        else if (strcmp(argv[i], "-t") == 0 || strcmp(argv[i], "--threads") == 0)
            T = util::aton(argc, argv, ++i);    // Number of parallel threads (default: auto)
        else if (strcmp(argv[i], "-v") == 0 || strcmp(argv[i], "--verbose") == 0)
            verbose = true;    // Print information messages during execution
        else if (strcmp(argv[i], "-V") == 0)    /* hidden option */
            verbose = true,    // Print some information messages during execution
            VERBOSE = true;    // Print more information messages during execution
        else if (!(strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0)) {
            $err << "Error: unknown argument: " << argv[i] << _end$;
            $err << "       type --help to see the full list of parameters" << _end$$;
        }
    }

    if (input.empty())
        $err << "Error: missing input argument: --input <file_name>" << _end$$;
    if (graph.empty())
        $err << "Error: missing input argument: --graph <file_name>" << _end$$;
    if (brake.empty())
        $warn << "Warning: missing input argument: --break <file_name>" << _end$,
        $warn << "         files can be generated using break_kmers.py" << _end$;

    if ( output.empty() && unimog.empty())
        $err << "Error: missing argument: --output/unimog <file_name>" << _end$$;
    if (!output.empty() && (speller::merge || speller::MERGE))
        $warn << "Warning: option --merge is not supported with --output" << _end$;
    if ( unimog.empty() && (speller::range || speller::RANGE))
        $warn << "Warning: option --range has no effect without --unimog" << _end$;

    if (T == 0 && ++T == 1)
        $warn << "Error: missing argument: number of threads -t <number>" << _end$;
    else if (verbose)
        $note << "Note: running RATS Speller using " << T << " threads" << _end$;

    // parse the list of input sequence files
    vector<string> file_names;
    {
        ifstream file(input);
        if (!file.good()) {
            $err << "Error: could not read input file: " << input << _end$$;
        }
        string line;
        while (getline(file, line)) {
            ifstream fastx(line);
            if (!fastx.good()) {
                $err << "Error: could not read sequence file: " << line << _end$$;
            }
            fastx.close();
            file_names.emplace_back(line);
        }
        if (file_names.size() > maxN) {
            $err << "Error: color number exceeds -DmaxN=" << maxN << ", please see makefile" << _end$$;
        }
        file.close();
    }

    // load an existing Bifrost graph file
    ColoredCDBG<ID> cdbg;
    if (cdbg.read(graph["graph_fn"], graph["color_fn"], T, verbose)) {
        if (verbose) $log << end$;
    } else {
        $err << "Error: could not load Bifrost graph: " << graph << _end$$;
    }
    if (speller::kmer != 0 && speller::kmer != cdbg.getK()) {
        $warn << "Warning: graph file does not match the given k-mer length" << _end$;
    } else {
        speller::kmer = cdbg.getK();
    }
    if (speller::kmer > maxK) {
        $err << "Error: k-mer length exceeds -DmaxK=" << maxK << ", please see makefile" << _end$$;
    }
    if (file_names.size() != cdbg.getNbColors()) {
        $err << "Error: list of sequence files and graph colors differ in size" << _end$$;
    }
    for (uint64_t i = 0; i < cdbg.getNbColors(); ++i) {
        if (file_names[i] != cdbg.getColorName(i)) {
            $err << "Error: list of sequence files and graph colors does not match" << _end$$;
        }
    }

    auto begin = chrono::high_resolution_clock::now();  // time measurement
    color::init(file_names.size());  // initialize the color number
    kmer::init(speller::kmer, "");  // initialize the k-mer length

    // load an existing list of breakpoint k-mers
    if (!brake.empty()) {
        if (verbose)
            $log $_ << "Reading breakpoints..." << $;
        {
            ifstream file(brake["L"]);
            kmer_t left_fw, right_rv;
            string line;
            while (getline(file, line)) {
                if (line.length() != speller::kmer)
                    $err $_ << "Error: break file does not match the given k-mer length" << _end$$;
                for (size_t k = 0; k < speller::kmer; ++k)
                    kmer::shift(left_fw, line[k]);
                speller::left_breakpoints.emplace(left_fw);
                right_rv = left_fw;
                kmer::reverse_complement(right_rv);
                speller::right_breakpoints.emplace(right_rv);
            }
            file.close();
        }{
            ifstream file(brake["R"]);
            kmer_t left_fw, right_rv;
            string line;
            while (getline(file, line)) {
                if (line.length() != speller::kmer)
                    $err $_ << "Error: break file does not match the given k-mer length" << _end$$;
                for (size_t k = 0; k < speller::kmer; ++k)
                    kmer::shift(left_fw, line[k]);
                speller::right_breakpoints.emplace(left_fw);
                right_rv = left_fw;
                kmer::reverse_complement(right_rv);
                speller::left_breakpoints.emplace(right_rv);
            }
            file.close();
        }
    }

    ofstream file(output);    // output file stream
    ostream stream(file.rdbuf());

    if (verbose)
        $log $_ << "Processing unitigs..." << $;
    uint64_t cur = 0, prog = 0, next;
    uint64_t max = cdbg.size();

    for (const auto& unitig : cdbg) {
        if (verbose) {
            next = 100 * cur / max;
             if (prog < next)  $log $_ << "Processed " << cur << " unitigs (" << next << "%) " << $;
            prog = next; cur++;
        }
        speller::process_unitig(unitig, stream);
        speller::process_component(stream);
    }
    if (verbose) {
        $log $_ << "Processed " << max << " unitigs (100%)" << end$;
    }
    file.close();

    if (!unimog.empty()) {
        vector<thread> _thread(T);  // parallel file reading threads
        atomic<uint64_t> I(0); mutex mutex;  // to sync the threads
       #ifndef DEBUG
         /**/ if (VERBOSE) $log $_ << "Reading input files..." << $;
         else if (verbose) $log $_ << "Reading input files..." << end$;
       #else
         /***************/ $log $_ << "Reading input files..." << end$;
       #endif

        auto lambda = [&] () {
        while (true) {  // select next file to process
            const size1N_t i = I++;  // atomic update
            if (i >= file_names.size()) break;

            ifstream fastx(file_names[i]);    // input file stream
            ostringstream name; name << unimog << '.';    // output file stream
            name << setw(floor(log10(file_names.size()))+1) << setfill('0') << i+1;
            ofstream file(name.str()); ostream stream(file.rdbuf());
            stream << "> " << file_names[i] << end$;    // next genome to be spelled

           #ifndef DEBUG
             /**/ if (VERBOSE) $SYNC( $log $_ << file_names[i] << " (" << i+1 << "/" << file_names.size() << ")" << end$ );
             else if (verbose) $SYNC( $log $_ << file_names[i] << " (" << i+1 << "/" << file_names.size() << ")" << $ );
           #else
             /**************/ $SYNC( $log << '\n' << file_names[i] << " (" << i+1 << "/" << file_names.size() << ")" << $ );
           #endif

            string line;    // read the file line by line
            string sequence;    // read in the sequence files and extract the k-mers
            if (getline(fastx, line)) {
                if (line.length() > 0) {
                    if (line[0] == '>' || line[0] == '@') {    // first FASTA & FASTQ header -> nothing to do yet
                       #ifndef DEBUG
                         /**/ if (VERBOSE) $SYNC( $lite $_ << line << _$ );    // print more fine-grained per-file progress
                         else if (verbose) $SYNC( $lite $_ << file_names[i] << " (" << i+1 << "/" << file_names.size() << ")" << _$ );
                       #else
                         /**************/ $SYNC( $lite << '\n' << line << _$ );    // print more fine-grained per-file progress
                       #endif
                    }
                }
            }
            while (getline(fastx, line)) {
                if (line.length() > 0) {
                    if (line[0] == '>' || line[0] == '@') {    // next FASTA & FASTQ header -> process prev. seq.
                        speller::process_chromosome(sequence, stream);
                        sequence.clear();

                       #ifndef DEBUG
                         /**/ if (VERBOSE) $SYNC( $lite $_ << line << _$ );    // print more fine-grained per-file progress
                         else if (verbose) $SYNC( $lite $_ << file_names[i] << " (" << i+1 << "/" << file_names.size() << ")" << _$ );
                       #else
                         /**************/ $SYNC( $lite << '\n' << line << _$ );    // print more fine-grained per-file progress
                       #endif
                    } else {
                        transform(line.begin(), line.end(), line.begin(), ::toupper);
                        // sequence += regex_replace(line, regex("[^ACGT]"), "");
                        sequence += line;    // FASTA & FASTQ sequence -> read
                    }
                }
            }
            speller::process_chromosome(sequence, stream);
            sequence.clear();

           #ifndef DEBUG
             if (verbose) $log $_ << $;
           #endif
            fastx.close();
            file.close();
        }};

        if (T == 1)   // single-CPU: execute everything in the main thread
            lambda();
        else {   // multi-CPU: distribute files to different threads
            for (uint64_t t = 0; t < T; ++t) _thread[t] = thread(lambda);
            for (uint64_t t = 0; t < T; ++t) _thread[t].join();
        }
    }

    auto end = chrono::high_resolution_clock::now();  // time measurement
   #ifndef DEBUG
     if (verbose)  // print progress and time
         $log << "Done! (" << util::format_time(end - begin) << ")" << end$;
   #endif
}
