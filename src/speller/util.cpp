#include "util.h"
#include "../ansi.h"

/**
 * This function prints a minimal version of the help page.
 */
void help::print_usage() {
    $out << "  Input arguments:" << end$;
    $out << end$;
    $out << "    -i, --input   \t Input FASTA files: list of sequence files, one per line" << end$;
    $out << "    -g, --graph   \t Input Graph files: load Bifrost graph, file name prefix" << end$;
    $out << "    -b, --break   \t Input Break files: list of left/right breakpoint k-mers" << end$;
    $out << end$;
    $out << "  Output arguments:" << end$;
    $out << end$;
    $out << "    -o, --output  \t Output Marker file: list of all unitig marker sequences" << end$;
    $out << "    -u, --unimog  \t Output UniMoG file: genomes spelled with unitig markers" << end$;
    $out << end$;
    $out << "  K-mer options:" << end$;
    $out << end$;
    $out << "    -k, --kmer    \t Length of k-mers (default: auto)" << end$;
    $out << end$;
    $out << "  Filter options:" << end$;
    $out << end$;
    $out << "    -c, --color   \t Discard single unitigs that have fewer than [n] colors" << end$;
    $out << "    -l, --length  \t Discard single unitigs that are shorter than [n] bases" << end$;
    $out << "    -m, --merge   \t Merge bubbling unitigs that are shorter than [n] bases" << end$;
    $out << "    -r, --range   \t Collapse consecutive runs of the same marker in UniMoG" << end$;
    $out << "    -s, --soft    \t Keep all (-l/c)-filtered-out unitigs with a marker '0'" << end$;
    $out << end$;
    $out << "  Other settings:" << end$;
    $out << end$;
    $out << "    -t, --threads \t Number of parallel threads (default: auto)" << end$;
    $out << "    -v, --verbose \t Print information messages during execution" << end$;
    $out << "    -h, --help    \t Display an extended help page and quit" << end$;
}

/**
 * This function prints an extended version of the help page.
 */
void help::print_extended_usage() {
    $out << " ___________________________________________________________________________" << end$;
    $out << end$;
    $out << "  Input arguments:" << end$;
    $out << end$;
    $out << "    -i, --input   \t Input FASTA files: list of sequence files, one per line" << end$;
    $out << end$;
    $out << "    -g, --graph   \t Input Graph files: load Bifrost graph, file name prefix" << end$;
    $out << "                  \t (requires the Bifrost library installed on the system)" << end$;
    $out << end$;
    $out << "    -b, --break   \t Input Break files: list of left/right breakpoint k-mers" << end$;
    $out << "                  \t (recommended, can be generated running break_kmers.py)" << end$;
    $out << " ___________________________________________________________________________" << end$;
    $out << end$;
    $out << "  Output arguments:" << end$;
    $out << end$;
    $out << "    -o, --output  \t Output Marker file: list of all unitig marker sequences" << end$;
    $out << end$;
    $out << "    -u, --unimog  \t Output UniMoG file: genomes spelled with unitig markers" << end$;
    $out << "                  \t (missing information about linear/circular chromosomes)" << end$;
    $out << " ___________________________________________________________________________" << end$;
    $out << end$;
    $out << "  K-mer options:" << end$;
    $out << end$;
    $out << "    -k, --kmer    \t Length of k-mers (default: auto)" << end$;
    $out << " ___________________________________________________________________________" << end$;
    $out << end$;
    $out << "  Filter options:" << end$;
    $out << end$;
    $out << "    -c, --color   \t Discard single unitigs that have fewer than [n] colors" << end$;
    $out << end$;
    $out << "    -l, --length  \t Discard single unitigs that are shorter than [n] bases" << end$;
    $out << end$;
    $out << "    -m, --merge   \t Merge bubbling unitigs that are shorter than [n] bases" << end$;
    $out << end$;
    $out << "    -r, --range   \t Collapse consecutive runs of the same marker in UniMoG" << end$;
    $out << end$;
    $out << "    -s, --soft    \t Keep all (-l/c)-filtered-out unitigs with a marker '0'" << end$;
    $out << " ___________________________________________________________________________" << end$;
    $out << end$;
    $out << "  Other settings:" << end$;
    $out << end$;
    $out << "    -t, --threads \t Number of parallel threads (default: auto)" << end$;
    $out << end$;
    $out << "    -v, --verbose \t Print information messages during execution" << end$;
    $out << end$;
    $out << "    -h, --help    \t Display an extended help page and quit" << end$;
    $out << " ___________________________________________________________________________" << end$;
}
