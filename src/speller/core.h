#include "../tsl/sparse_set.h"
#include "../tsl/sparse_map.h"

#include <bifrost/CompactedDBG.hpp>
#include <bifrost/ColoredCDBG.hpp>
#include <queue>

#ifndef MAX_KMER_SIZE
    #define MAX_KMER_SIZE (maxK+1)
#endif

#include "../color.h"
#include "kmer.h"
using namespace std;

template <typename T>
  using hash_set = tsl::sparse_pg_set<T>;
template <typename K, typename V>
  using hash_map = tsl::sparse_pg_map<K,V>;

class ID: public CCDBG_Data_t<ID> {
 public:
    int64_t value  = 0;
    bool reference = false;
};
#define unitig_t UnitigMap<DataAccessor<ID>, DataStorage<ID>, 0>
#define unitig_id(unitig) unitig.getData()->getData(unitig)->value
#define unitig_ref(unitig) unitig.getData()->getData(unitig)->reference

/**
 * This class manages the core functionalities of the speller.
 */
class speller {
 private:
    static int64_t new_marker_id;

    static queue<unitig_t> neighbor_queue;
    static queue<unitig_t> component_queue;

    static hash_map<kmer_t, const pair<const int64_t, const uint64_t>> unitig_table;
    static hash_map<int64_t, pair<uint64_t, long double>> component_table;

 public:
    static uint64_t kmer;    // Length of k-mers (default: auto)

    static uint64_t color;    // Discard single unitigs with less than [n] colors
    static uint64_t COLOR;    // Discard core components with less than [n] colors
    static uint64_t length;   // Discard single unitigs shorter than [n] bases
    static uint64_t LENGTH;   // Discard core components shorter than [n] bases

    static bool     soft;     // Keep (-l/c)-filtered-out unitigs with marker '0'
    static bool     SOFT;     // Keep skipped (not found) k-mers with marker '0'

    static uint64_t merge;    // Merge bubbling unitigs shorter than [n] bases
    static bool     MERGE;    // Represent core connected components by the same marker
    static bool     range;    // Collapse consecutive runs of the same marker in UniMoG
    static bool     RANGE;    // Collapse alternating runs of the same marker in UniMoG

    static hash_set<kmer_t> left_breakpoints;
    static hash_set<kmer_t> right_breakpoints;

    /**
     * This function hashes the unitig begin & end k-mers and finds the connected components.
     *
     * @param unitig Bifrost graph unitig
     * @param stream output Marker stream
     */
    static void process_unitig(const unitig_t& unitig, ostream& stream);

    /**
     * This function hashes the unitig begin & end k-mers in the current connected component.
     *
     * @param stream output Marker stream
     */
    static void process_component(ostream& stream);

    /**
     * This function spells a chromosome replacing the sequence with matching unitig markers.
     *
     * @param sequence chromosome sequence
     * @param stream output UniMoG stream
     */
    static void process_chromosome(const string& sequence, ostream& stream);

protected:
    /**
     * This function assigns a new marker ID to a unitig and finds the connected components.
     *
     * @param unitig Bifrost graph unitig
     */
    static void new_component(const unitig_t& unitig);

    /**
     * This array can be used to test if a string character is a valid or invalid DNA base.
     */
    constexpr static bool invalid_char[256]
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
         1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
};
