#include <cstring>
#include <iomanip>
#include <thread>

#include "core.h"
#include "util.h"
using namespace std;

// Rearrangement-Aware Trees form Sequences
#define RATS_VERSION "0.23_06E"    // RATS

/**
 * This is the entry point of the program.
 *
 * @param argc number of cmd args
 * @param argv cmd args
 * @return exit status
 */
int main(int argc, char* argv[]);
