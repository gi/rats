#include "core.h"
#include "../ansi.h"

/* ------------------------------------------------------------------------------------- */

uint64_t speller::kmer(0);    // Length of k-mers (default: auto)

uint64_t speller::color(0);    // Discard single unitigs with less than [n] colors
uint64_t speller::COLOR(0);    // Discard core components with less than [n] colors
uint64_t speller::length(0);   // Discard single unitigs shorter than [n] bases
uint64_t speller::LENGTH(0);   // Discard core components shorter than [n] bases

bool     speller::soft(false);   // Keep (-l/c)-filtered-out unitigs with marker '0'
bool     speller::SOFT(false);   // Keep skipped (not found) k-mers with marker '0'

uint64_t speller::merge(0);       // Merge bubbling unitigs shorter than [n] bases
bool     speller::MERGE(false);   // Represent core connected components by the same marker
bool     speller::range(false);   // Collapse consecutive runs of the same marker in UniMoG
bool     speller::RANGE(false);   // Collapse alternating runs of the same marker in UniMoG

hash_set<kmer_t> speller::left_breakpoints;
hash_set<kmer_t> speller::right_breakpoints;

/* ------------------------------------------------------------------------------------- */

int64_t speller::new_marker_id(0);

queue<unitig_t> speller::neighbor_queue;
queue<unitig_t> speller::component_queue;

hash_map<kmer_t, const pair<const int64_t, const uint64_t>> speller::unitig_table;
hash_map<int64_t, pair<uint64_t, long double>> speller::component_table;

const bool speller::invalid_char[256];

/* ------------------------------------------------------------------------------------- */

/**
 * This function hashes the unitig begin & end k-mers and finds the connected components.
 *
 * @param unitig Bifrost graph unitig
 * @param stream output Marker stream
 */
void speller::process_unitig(const unitig_t& unitig, ostream& stream) {
    if (unitig_ref(unitig)) return; else unitig_ref(unitig) = true;

    auto sequence = unitig.mappedSequenceToString();
    auto* matrix = unitig.getData()->getUnitigColors(unitig);
    vector<color_t> colors(sequence.length()-kmer+1, 0);
    for (auto it = matrix->begin(unitig); it != matrix->end(); ++it) {
        colors[it.getKmerPosition()].set(it.getColorID());
    }
    kmer_t left_fw, current_kmer, next_kmer, right_rv;
    for (size_t pos = 0; pos < kmer; ++pos) {
        kmer::shift(current_kmer, sequence[pos]);
    }

    left_fw = current_kmer;
    size_t start_pos = 0;
    uint64_t current_marker_length = kmer;
    color_t current_color = colors[0];
    for (size_t pos = kmer; pos < sequence.length(); ++pos) {
        next_kmer = current_kmer;
        kmer::shift(next_kmer, sequence[pos]);
        if (current_color != colors[pos-kmer+1] || right_breakpoints.find(current_kmer) != right_breakpoints.end()
                                                || left_breakpoints.find(next_kmer) != left_breakpoints.end()) {
            right_rv = current_kmer;
            kmer::reverse_complement(right_rv);
            uint64_t current_color_count = current_color.popcnt();
            if (current_marker_length >= length && current_color_count >= color) {
                if (MERGE) {
                    if (!unitig_id(unitig)) unitig_id(unitig) = ++new_marker_id, new_component(unitig);
                    unitig_table.emplace(left_fw, make_pair(+unitig_id(unitig), current_marker_length));
                    unitig_table.emplace(right_rv, make_pair(-unitig_id(unitig), current_marker_length));
                    component_table[new_marker_id].first += current_marker_length-kmer+1;
                    component_table[new_marker_id].second += (current_marker_length-kmer+1) * current_color_count;
                } else {
                    int64_t current_marker_id = ++new_marker_id;
                    unitig_table.emplace(left_fw, make_pair(+current_marker_id, current_marker_length));
                    unitig_table.emplace(right_rv, make_pair(-current_marker_id, current_marker_length));
                    stream << sequence.substr(start_pos, current_marker_length) << end$;
                }
            } else {
                unitig_table.emplace(left_fw, make_pair(+0, current_marker_length));
                unitig_table.emplace(right_rv, make_pair(-0, current_marker_length));
            }
            current_kmer = next_kmer;
            left_fw = current_kmer;
            start_pos = pos-kmer+1;
            current_marker_length = kmer;
            current_color = colors[pos-kmer+1];
        } else {
            current_kmer = next_kmer;
            current_marker_length++;
        }
    }
    right_rv = current_kmer;
    kmer::reverse_complement(right_rv);
    uint64_t current_color_count = current_color.popcnt();
    if (current_marker_length >= length && current_color_count >= color) {
        if (MERGE) {
            if (!unitig_id(unitig)) unitig_id(unitig) = ++new_marker_id, new_component(unitig);
            unitig_table.emplace(left_fw, make_pair(+unitig_id(unitig), current_marker_length));
            unitig_table.emplace(right_rv, make_pair(-unitig_id(unitig), current_marker_length));
            component_table[new_marker_id].first += current_marker_length-kmer+1;
            component_table[new_marker_id].second += (current_marker_length-kmer+1) * current_color_count;
        } else {
            int64_t current_marker_id = ++new_marker_id;
            unitig_table.emplace(left_fw, make_pair(+current_marker_id, current_marker_length));
            unitig_table.emplace(right_rv, make_pair(-current_marker_id, current_marker_length));
            stream << sequence.substr(start_pos, current_marker_length) << end$;
        }
    } else {
        unitig_table.emplace(left_fw, make_pair(+0, current_marker_length));
        unitig_table.emplace(right_rv, make_pair(-0, current_marker_length));
    }
}

/**
 * This function assigns a new marker ID to a unitig and finds the connected components.
 *
 * @param unitig Bifrost graph unitig
 */
void speller::new_component(const unitig_t& unitig) {
    component_table.emplace(new_marker_id, make_pair(0, 0));

    for (const auto& succ : unitig.getSuccessors())
        if (!unitig_id(succ)) unitig_id(succ) = new_marker_id, neighbor_queue.emplace(succ);
    for (const auto& pred : unitig.getPredecessors())
        if (!unitig_id(pred)) unitig_id(pred) = new_marker_id, neighbor_queue.emplace(pred);

    while (!neighbor_queue.empty()) {
        const auto& neighbor = neighbor_queue.front();
        component_queue.emplace(neighbor); neighbor_queue.pop();

        for (const auto& succ: neighbor.getSuccessors())
            if (!unitig_id(succ)) unitig_id(succ) = new_marker_id, neighbor_queue.emplace(succ);
        for (const auto& pred: neighbor.getPredecessors())
            if (!unitig_id(pred)) unitig_id(pred) = new_marker_id, neighbor_queue.emplace(pred);
    }
}

/**
 * This function hashes the unitig begin & end k-mers in the current connected component.
 *
 * @param stream output Marker stream
 */
void speller::process_component(ostream& stream) {
    while (!component_queue.empty()) {
        process_unitig(component_queue.front(), stream);
        component_queue.pop();
    }
    component_table[new_marker_id].second /= component_table[new_marker_id].first;
    component_table[new_marker_id].first += kmer-1;
}

/**
 * This function spells a chromosome replacing the sequence with matching unitig markers.
 *
 * @param sequence chromosome sequence
 * @param stream output UniMoG stream
 */
void speller::process_chromosome(const string& sequence, ostream& stream) {
   #ifdef DEBUG
     $log << " [" << sequence.length() << "] " << $;
   #endif
    kmer_t current_kmer; size_t pos = 0;
    int64_t current_marker_id = INT64_MIN;
  begin:
    for (size_t k = 0; k < kmer; ++k) {
        if (pos >= sequence.length())
          {  /* $log << '^' << $; */ goto end;  }
        if (invalid_char[sequence[pos]])
          {  ++pos; goto begin;  }
        kmer::shift(current_kmer, sequence[pos++]);
    }
   #ifdef DEBUG
     $log << ' ' << pos-kmer << '.' << $;
   #endif
    while (true) {
        auto marker = unitig_table.find(current_kmer);
        if (marker != unitig_table.end()) {
            bool condition1 = !MERGE && marker->second.first;
            bool condition2 =  MERGE && marker->second.first && component_table[llabs(marker->second.first)].first >= LENGTH
                                                             && component_table[llabs(marker->second.first)].second >= COLOR;
            if (soft && !condition1 && !condition2) {
                if (!range && !RANGE || range && 0 != current_marker_id || RANGE && 0 != current_marker_id) {
                    current_marker_id = 0;
                    stream << current_marker_id << ' ';
                }
            } else if (condition1 || condition2) {
                if (!range && !RANGE || range && marker->second.first != current_marker_id
                                     || RANGE && llabs(marker->second.first) != llabs(current_marker_id)) {
                    current_marker_id = marker->second.first;
                    stream << current_marker_id << ' ';
                }
            }
            for (size_t k = kmer; k < marker->second.second; ++k) {
                if (pos >= sequence.length())
                  {  /* $log << '%' << $; */ goto end;  }
                if (invalid_char[sequence[pos]])
                  {  ++pos; goto begin;  }
                kmer::shift(current_kmer, sequence[pos++]);
            }
           #ifdef DEBUG
             $log << '_' << pos-1 << ' ' << $;
           #endif
        } else if (SOFT) {
            if (!range && !RANGE || range && 0 != current_marker_id || RANGE && 0 != current_marker_id) {
                current_marker_id = 0;
                stream << current_marker_id << ' ';
            }
        }
        if (pos >= sequence.length())
          {  /* $log << '$' << $; */ goto end;  }
        if (invalid_char[sequence[pos]])
          {  ++pos; goto begin;  }
        kmer::shift(current_kmer, sequence[pos++]);
       #ifdef DEBUG
         $log << ' ' << pos-kmer << '.' << $;
       #endif
    }
  end:
    stream << end$;
}
