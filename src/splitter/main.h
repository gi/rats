#include <cstring>
#include <iomanip>
#include <thread>

#include "core.h"
#include "util.h"

#include <bifrost/CompactedDBG.hpp>
#include <bifrost/ColoredCDBG.hpp>
#ifndef MAX_KMER_SIZE
    #define MAX_KMER_SIZE (maxK+1)
#endif

using namespace std;

// Rearrangement-Aware Trees form Sequences
#define RATS_VERSION "0.23_06B"    // RATS

/**
 * This is the entry point of the program.
 *
 * @param argc number of cmd args
 * @param argv cmd args
 * @return exit status
 */
int main(int argc, char* argv[]);
