#include "main.h"
#include "../ansi.h"

/**
 * This is the entry point of the program.
 *
 * @param argc number of cmd args
 * @param argv cmd args
 * @return exit status
 */
int main(int argc, char* argv[]) {
    ios_base::sync_with_stdio(false);

    // check for a new version of RATS Speller at program start
    if (!system("wget --timeout=1 --tries=1 -qO- https://gitlab.ub.uni-bielefeld.de/gi/rats/raw/main/src/splitter/main.h | grep -q RATS_VERSION")
      && system("wget --timeout=1 --tries=1 -qO- https://gitlab.ub.uni-bielefeld.de/gi/rats/raw/main/src/splitter/main.h | grep -q " RATS_VERSION)) {
        $link << "NEW VERSION AVAILABLE: https://gitlab.ub.uni-bielefeld.de/gi/rats" << _end$;
    }
    // print a help message describing the program arguments
    if (argc <= 1 || strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0) {
        $out << end$;
        $out << "RATS Splitter | version " << RATS_VERSION << end$;
        $out << "Usage: Splitter [PARAMETERS]" << end$;
        $out << end$;
        if (argc <= 1) help::print_usage();
        else           help::print_extended_usage();
        $out << end$$;
    }

    file input;    // Input FASTA files: list of sequence files, one per line
    file unimog;   // Input UniMoG files: genomes spelled with unitig markers
    file splits;   // Output TSV file: list of splits, sorted by weight desc.

    uint64_t length = 0;   // Length of marker tuples (default: -DmaxL)
    string   pattern;      // Pattern of gapped tuples (default: no gaps)
    uint64_t quality = 1;  // Discard tuples with lower coverage than [n]
    bool     reverse = 0;  // Keep one repr. for reverse complement tuples

    uint64_t top = -1;     // Number of splits in the output list (default: all)
    string   filter;       // Output a greedy maximum weight subset of splits
    bool     verbose = 0;  // Print some information messages during execution
    bool     VERBOSE = 0;  // Print more information messages during execution

    uint64_t T = 0;    // Number of parallel threads (default: auto)
    uint64_t P = 0;    // Number of file reading threads (default: auto)
    uint64_t Q = 0;    // Number of queue hashing threads (default: auto)

    auto arithmetic_mean = [] (const uint32_t& x, const uint32_t& y) { return x / 2.0 + y / 2.0; };
    auto geometric_mean  = [] (const uint32_t& x, const uint32_t& y) { return sqrt(x) * sqrt(y); };
    auto geometric_mean2 = [] (const uint32_t& x, const uint32_t& y) { return sqrt(x+1) * sqrt(y+1) - 1; };
    function<double(const uint32_t&, const uint32_t&)> default_mean  = geometric_mean;

    // parse the command line arguments and update the variables above
    for (int i = 1; i < argc; ++i) {
        if (strcmp(argv[i], "-i") == 0 || strcmp(argv[i], "--input") == 0)
            input = file(util::atos(argc, argv, ++i), type::INPUT_FILE, mode::READ);
        else if (strcmp(argv[i], "-u") == 0 || strcmp(argv[i], "--unimog") == 0)
            unimog = file(util::atos(argc, argv, ++i), type::UNIMOG_FILE, mode::READ);
        else if (strcmp(argv[i], "-s") == 0 || strcmp(argv[i], "--splits") == 0)
            splits = file(util::atos(argc, argv, ++i), type::SPLITS_FILE, mode::WRITE);

        else if (strcmp(argv[i], "-l") == 0 || strcmp(argv[i], "--length") == 0)
            length = util::aton(argc, argv, ++i);    // Length of marker tuples (default: -DmaxL)
        else if (strcmp(argv[i], "-g") == 0 || strcmp(argv[i], "--gapped") == 0)
            pattern = util::atos(argc, argv, ++i);    // Pattern of gapped tuples (default: no gaps)
        else if (strcmp(argv[i], "-q") == 0 || strcmp(argv[i], "--qualify") == 0)
            quality = util::aton(argc, argv, ++i);    // Discard tuples below a coverage threshold
        else if (strcmp(argv[i], "-r") == 0 || strcmp(argv[i], "--reverse") == 0)
            reverse = true;    // Keep one repr. for reverse complement tuples

        else if (strcmp(argv[i], "-t") == 0 || strcmp(argv[i], "--top") == 0)
            top = util::aton(argc, argv, ++i);    // Number of top splits to output (default: all)
        else if (strcmp(argv[i], "-m") == 0 || strcmp(argv[i], "--mean") == 0) {
            string mean = util::atos(argc, argv, ++i);    // Mean weight function to handle asymmetric splits
            if      (mean == "arith") default_mean = arithmetic_mean;
            else if (mean == "geom")  default_mean = geometric_mean;
            else if (mean == "geom2") default_mean = geometric_mean2;
            else {
                $err << "Error: unknown argument: " << argv[i-1] << ' ' << mean << _end$;
                $err << "       type --help to see a list of supported options" << _end$$;
           }}
        else if (strcmp(argv[i], "-f") == 0 || strcmp(argv[i], "--filter") == 0) {
            filter = util::atos(argc, argv, ++i);    // Output a greedy maximum weight subset of splits
            if      (filter == "strict" || filter == "tree"); // compatible to a tree
            else if (filter == "weakly");                     // weakly compatible network
            else if (filter.find("tree") != -1 && filter.substr(filter.find("tree")) == "tree")
                util::ston(argv[i-1], filter);
            else {
                $err << "Error: unknown argument: " << argv[i-1] << ' ' << filter << _end$;
                $err << "       type --help to see a list of supported options" << _end$$;
           }}

        else if (strcmp(argv[i], "-p") == 0 || strcmp(argv[i], "--threads") == 0)
            T = util::aton(argc, argv, ++i);    // Number of parallel threads (default: auto)
        else if (strcmp(argv[i], "-P") == 0) {    /* hidden option */
            string arg = util::atos(argc, argv, ++i);    // Number of threads... (default: auto)
            if (arg.find('+') > 0 && arg.find('+') < arg.size()-1)
                P = util::ston(argv[i-1], arg.substr(0, arg.find('+'))),    // file reading
                Q = util::ston(argv[i-1], arg.substr(arg.find('+')+1));    // queue hashing
            else {
                $err << "Error: unknown argument: " << argv[i-1] << ' ' << arg << _end$;
                $err << "       type --help to see a list of supported options" << _end$$;
           }}

        else if (strcmp(argv[i], "-v") == 0 || strcmp(argv[i], "--verbose") == 0)
            verbose = true;    // Print information messages during execution
        else if (strcmp(argv[i], "-rv") == 0 || strcmp(argv[i], "-vr") == 0)
            reverse = true,    // Keep one repr. for reverse complement tuples
            verbose = true;    // Print information messages during execution
        else if (strcmp(argv[i], "-V") == 0)    /* hidden option */
            verbose = true,    // Print some information messages during execution
            VERBOSE = true;    // Print more information messages during execution
        else if (!(strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0)) {
            $err << "Error: unknown argument: " << argv[i] << _end$;
            $err << "       type --help to see the full list of parameters" << _end$$;
        }
    }

    if (input.empty())
        $err << "Error: missing input argument: --input <file_name>" << _end$$;
    if (unimog.empty())
        $err << "Error: missing input argument: --unimog <file_name>" << _end$$;
    if (splits.empty())
        $err << "Error: missing output argument: --splits <file_name>" << _end$$;

    if (!pattern.empty()) {
        uint64_t markers = 0;    // length without gaps
        for (size_t i = 0; i < pattern.length(); ++i) {
            if (pattern[i] == '1')
               { ++markers; continue; }
            if (pattern[i] != '0')
                $err << "Error: pattern must be a sequence of 0s and 1s, where 0 means a gap" << _end$$;
            if (reverse && pattern[i] != pattern[pattern.length()-1-i])
                $err << "Error: pattern should be symmetric to work with reverse complements" << _end$$;
        }
        if (length != 0 && length != markers && length != pattern.length())
            $err << "Error: pattern length does not match the given tuple length" << _end$$;
        length = pattern.length();

        if (markers == 0)
            $err << "Error: pattern must be a sequence of 0s and 1s, with at least one 1" << _end$$;
        if (markers == pattern.length())
            pattern.clear();
    }
    if (length == 0) {
        length = maxL;    // tuple length not specified & not defined by pattern
    } else if (length > maxL) {
        $err << "Error: tuple length exceeds -DmaxL=" << maxL << ", please see makefile" << _end$$;
    }

    // parse the list of input sequence files
    vector<string> file_names;
    {
        ifstream file(input);
        if (!file.good()) {
            $err << "Error: could not read input file: " << input << _end$$;
        }
        string line;
        while (getline(file, line)) {
            file_names.emplace_back(line);
        }
        if (file_names.size() > maxN) {
            $err << "Error: color number exceeds -DmaxN=" << maxN << ", please see makefile" << _end$$;
        }
        file.close();
    }

    if (T == 0) T = P + Q;   // users can provide either a total number for T or separate values for P & Q
    if (T == 0) T = thread::hardware_concurrency();   // if not, auto-detect max. number of hardware-threads
    if (T == 0) $err << "Error: missing argument: number of parallel threads -p/--threads <number>" << _end$$;

    if (P == 0 && Q == 0) {    // distribute file reading (P) and queue hashing (Q) threads
        if (T == 1) P = 1, Q = 0;    // special case: only a single thread, don't use queues
        if (T >= 2) {                // default case: find the best ratio of P and Q threads
            double PQ_ratio = 0.32142857142857145;    // empirical factor for different configs
            if (!pattern.empty()) PQ_ratio = 0.39285714285714285;
            if (quality > 1)      PQ_ratio = 1.0;

            P = min(max<uint64_t>(1, round(PQ_ratio * T)), T-1);
            for (uint64_t I = (file_names.size() / P) + (bool) (file_names.size() % P); P > 1
                       && I == (file_names.size() / (P-1)) + (bool) (file_names.size() % (P-1)); P--);
            Q = T - P;    // correction based on the actual number of files to read
        }
    }

    auto begin = chrono::high_resolution_clock::now();  // time measurement
    color::init(file_names.size());  // initialize the color number
    kmer::init(length, pattern);  // initialize the tuple length
    splitter::init(top, quality, reverse, P, Q);  // initialize the core parameters

    if (Q == 0 && verbose)
        $note << "Note: running RATS Splitter using 1 thread" << _end$;
    if (Q >= 1 && verbose)
        $note << "Note: running RATS Splitter using " << (P+Q) << " (" << P << '+' << Q << ") threads" << _end$;
    if (P >= 2 && quality > 1)
        $warn << "Warning: using --qualify on multiple threads could increase memory usage" << _end$,
        $warn << "         In case of problems, please specify a smaller number of threads" << _end$;

    vector<thread> P_thread(P);  // parallel file reading threads
    vector<thread> Q_thread(Q);  // parallel queue hashing threads
    atomic<uint64_t> I(0), active(P); mutex mutex;  // sync threads
         if (VERBOSE) $log $_ << "Reading input files..." << $;
    else if (verbose) $log $_ << "Reading input files..." << end$;

    auto P_lambda = [&] (const uint64_t& T) {
        while (true) {  // select next file to process
            const size1N_t i = I++;  // atomic update
            if (i >= file_names.size()) break;

            ostringstream name; name << unimog << '.';
            name << setw(floor(log10(file_names.size()))+1) << setfill('0') << i+1;
            ifstream file(name.str());    // input file stream & print progress
                 if (VERBOSE) $SYNC( $log $_ << file_names[i] << " (" << i+1 << "/" << file_names.size() << ")" << end$ );
            else if (verbose) $SYNC( $log $_ << file_names[i] << " (" << i+1 << "/" << file_names.size() << ")" << $ );

            string line;    // read the file line by line and extract the k-mers
            if (getline(file, line)) {
                if (line.length() > 0) {
                    if (line[0] == '>' || line[0] == '@') {}    // first FASTA & FASTQ header -> nothing to do yet
                }
            }
            while (getline(file, line)) {
                if (line.length() > 0) {
                    if (VERBOSE) $SYNC( $lite $_ << file_names[i] << " (" << i+1 << "/" << file_names.size() << ")" << _$ );
                    splitter::add_kmers(T, line, i);    // print more fine-grained per-file progress
                }
            }
            if (verbose)
                $log $_ << $;
            file.close();

            splitter::clear_filter(T);
        } --active;
    };
    auto Q_lambda = [&] (const uint64_t& T) {
        while (active) {   // wait for threads
            splitter::merge_thread(T);
            this_thread::sleep_for(1ns);
        }   splitter::merge_thread(T);
    };

    if (Q == 0)   // single-CPU: execute everything in the main thread
        P_lambda(0);
    else {   // multi-CPU: distribute file reading & queue hashing threads
        for (uint64_t p = 0; p < P; ++p) P_thread[p] = thread(P_lambda, p);
        for (uint64_t q = 0; q < Q; ++q) Q_thread[q] = thread(Q_lambda, q);
        for (uint64_t p = 0; p < P; ++p) P_thread[p].join();
        for (uint64_t q = 0; q < Q; ++q) Q_thread[q].join();
    }
    splitter::erase_filters();

    if (verbose)
        $log $_ << "Processing splits..." << $;
    splitter::calc_weights(default_mean, verbose);    // accumulate split weights

    if (!filter.empty()) {    // apply filter
        if (verbose)
            $log $_ << "Filtering splits..." << $;
        if (filter == "strict" || filter == "tree")
            splitter::filter_strict(verbose);
        else if (filter == "weakly")
            splitter::filter_weakly(verbose);
        else if (filter.find("tree") != -1 && filter.substr(filter.find("tree")) == "tree")
            splitter::filter_n_tree(stol(filter), verbose);
    }

    if (verbose)
        $log $_ << "Please wait... " << $;
    ofstream file(splits);    // output file stream
    ostream stream(file.rdbuf());
    // stream << fixed;
    for (auto& split : splitter::splits) {
        stream << split.first;    // weight of the split
        for (size1N_t i = 0; i < file_names.size(); ++i) {
            if (split.second.test(i)) {
                stream << '\t' << file_names[i];    // name of the file
            }
        }
        stream << endl;
    }
    file.close();

    auto end = chrono::high_resolution_clock::now();  // time measurement
    if (verbose)  // print progress and time
        $log << "Done! (" << util::format_time(end - begin) << ")" << end$;
}
