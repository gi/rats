#include "kmer.h"
#include "../ansi.h"

/*
 * This class contains functions for working with marker tuples.
 */
size_t kmer::k;    // length of a tuple (including gap positions)
bool   kmer::mask[maxL];   // mask to erase certain markers in a gapped tuple pattern
bool   kmer::gapped;    // indicates if a gap pattern was specified

/**
 * This function initializes the tuple length and masks.
 *
 * @param length tuple length
 * @param pattern gapped tuple pattern
 */
void kmer::init(const size_t& length, const string& pattern) {
    k = length; gapped = !pattern.empty();

    if (pattern.empty()) {
        for (size_t i = 0; i < k; ++i)
            mask[i] = true;
    } else {
        for (size_t i = 0; i < k; ++i)
            if (pattern[i]-48) mask[i] = true;
    }
}

/**
 * This function shifts a tuple appending a new marker to the right.
 *
 * @param tuple current tuple
 * @param chr next marker
 */
void kmer::shift(kmer_t& tuple, const int64_t& marker) {
    for (size_t i = k-1; i != 0; --i)
        tuple[i] = tuple[i-1];
    tuple[0] = marker;
}

/**
 * This function unshifts a tuple returning the marker on the right.
 *
 * @param kmer current tuple
 * @param chr right marker
 */
void kmer::unshift(kmer_t& tuple, int64_t& marker) {
    marker = tuple[0];
    for (size_t i = k-1; i != 0; --i)
        tuple[i-1] = tuple[i];
    tuple[k-1] = 0;
}

/**
 * This function constructs the reverse complement of a given tuple.
 *
 * @param tuple current tuple
 */
void kmer::reverse_complement(kmer_t& tuple) {
    int64_t temp;
    for (size_t i = 0; i < (k+1)/2; ++i) {
        temp = tuple[i];
        tuple[i] = -tuple[k-i-1];
        tuple[k-i-1] = -temp;
    }
}

/**
 * This function constructs the r.c. representative of a given tuple.
 *
 * @param tuple current tuple
 * @return 1 if inverted, 0 otherwise
 */
bool kmer::reverse_represent(kmer_t& tuple) {
    for (size_t i = 0; i < (k+1)/2; ++i) {
        if (tuple[i] < -tuple[k-i-1])
            return false;
    }
    int64_t temp;
    for (size_t i = 0; i < (k+1)/2; ++i) {
        temp = tuple[i];
        tuple[i] = -tuple[k-i-1];
        tuple[k-i-1] = -temp;
    }
    return true;
}

/**
 * This function applies a gap pattern to the tuple.
 *
 * @param tuple current tuple
 */
void kmer::extract_pattern(kmer_t& tuple) {
    for (size_t i = 0; i < k; ++i) {
        if (!mask[i]) tuple[i] = 0;
    }
}
