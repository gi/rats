#include "util.h"
#include "../ansi.h"

/**
 * This function prints a minimal version of the help page.
 */
void help::print_usage() {
    $out << "  Input arguments:" << end$;
    $out << end$;
    $out << "    -i, --input   \t Input FASTA files: list of sequence files, one per line" << end$;
    $out << "    -u, --unimog  \t Input UniMoG files: genomes spelled with unitig markers" << end$;
    $out << end$;
    $out << "  Output arguments:" << end$;
    $out << end$;
    $out << "    -s, --splits  \t Output TSV file: list of splits, sorted by weight desc." << end$;
    $out << end$;
    $out << "  K-mer options:" << end$;
    $out << end$;
    $out << "    -l, --length  \t Length of marker tuples (default: -DmaxL)" << end$;
    $out << "    -g, --gapped  \t Pattern of gapped tuples (default: no gaps)" << end$;
    $out << "    -q, --qualify \t Discard tuples with lower coverage than [n]" << end$;
    $out << "    -r, --reverse \t Keep one repr. for reverse complement tuples" << end$;
    $out << end$;
    $out << "  Filter options:" << end$;
    $out << end$;
    $out << "    -t, --top     \t Number of splits in the output list (default: all)" << end$;
    $out << "    -m, --mean    \t Mean weight function to handle asymmetric splits" << end$;
    $out << "    -f, --filter  \t Output a greedy maximum weight subset of splits" << end$;
    $out << end$;
    $out << "  Other settings:" << end$;
    $out << end$;
    $out << "    -p, --threads \t Number of parallel threads (default: auto)" << end$;
    $out << "    -v, --verbose \t Print information messages during execution" << end$;
    $out << "    -h, --help    \t Display an extended help page and quit" << end$;
}

/**
 * This function prints an extended version of the help page.
 */
void help::print_extended_usage() {
    $out << " ___________________________________________________________________________" << end$;
    $out << end$;
    $out << "  Input arguments:" << end$;
    $out << end$;
    $out << "    -i, --input   \t Input FASTA files: list of sequence files, one per line" << end$;
    $out << end$;
    $out << "    -u, --unimog  \t Input UniMoG files: genomes spelled with unitig markers" << end$;
    $out << "                  \t (missing information about linear/circular chromosomes)" << end$;
    $out << " ___________________________________________________________________________" << end$;
    $out << end$;
    $out << "  Output arguments:" << end$;
    $out << end$;
    $out << "    -s, --splits  \t Output TSV file: list of splits, sorted by weight desc." << end$;
    $out << " ___________________________________________________________________________" << end$;
    $out << end$;
    $out << "  K-mer options:" << end$;
    $out << end$;
    $out << "    -l, --length  \t Length of marker tuples (default: -DmaxL)" << end$;
    $out << end$;
    $out << "    -g, --gapped  \t Pattern of gapped tuples (default: no gaps)" << end$;
    $out << "                  \t e.g. 11011, where 0 means a gap/skip marker" << end$;
    $out << end$;
    $out << "    -q, --qualify \t Discard tuples with lower coverage than [n]" << end$;
    $out << "                  \t Only count multi-occurrence tuples per file" << end$;
    $out << end$;
    $out << "    -r, --reverse \t Keep one repr. for reverse complement tuples" << end$;
    $out << "                  \t Reverse and consider the lex. smaller of both" << end$;
    $out << " ___________________________________________________________________________" << end$;
    $out << end$;
    $out << "  Filter options:" << end$;
    $out << end$;
    $out << "    -t, --top     \t Number of splits in the output list (default: all)" << end$;
    $out << end$;
    $out << "    -m, --mean    \t Mean weight function to handle asymmetric splits" << end$;
    $out << "                  \t options: arith: arithmetic mean" << end$;
    $out << "                  \t          geom:  geometric mean (default)" << end$;
    $out << "                  \t          geom2: geometric mean with pseudo-counts" << end$;
    $out << end$;
    $out << "    -f, --filter  \t Output a greedy maximum weight subset of splits" << end$;
    $out << "                  \t options: strict: compatible to a tree" << end$;
    $out << "                  \t          weakly: weakly compatible network" << end$;
    $out << "                  \t          n-tree: compatible to a union of n trees" << end$;
    $out << "                  \t                  (where n is an arbitrary number)" << end$;
    $out << " ___________________________________________________________________________" << end$;
    $out << end$;
    $out << "  Other settings:" << end$;
    $out << end$;
    $out << "    -p, --threads \t Number of parallel threads (default: auto)" << end$;
    $out << "                  \t Reduces runtime, but could increase memory usage" << end$;
    $out << "                  \t In case of problems, please try a smaller number" << end$;
    $out << end$;
    $out << "    -v, --verbose \t Print information messages during execution" << end$;
    $out << end$;
    $out << "    -h, --help    \t Display an extended help page and quit" << end$;
    $out << " ___________________________________________________________________________" << end$;
}
