#include "../mcl/concurrent_queue.h"
#include "../tsl/sparse_set.h"
#include "../tsl/sparse_map.h"
#include "../color.h"
#include "kmer.h"

#include <set>
#include <sstream>
using namespace std;

template <typename K, typename V>
  using queue = mcl::concurrent_queue<pair<K,V>>;
template <typename T>
  using hash_set = tsl::sparse_pg_set<T>;
template <typename K, typename V>
  using hash_map = tsl::sparse_pg_map<K,V>;

template <typename K, typename V>
  struct compare: public function<bool(pair<K,V>, pair<K,V>)> {
      constexpr bool operator()(const pair<K,V>& x, const pair<K,V>& y) const noexcept {
          return x.first > y.first || x.first == y.first && x.second < y.second;
      }
  };
template <typename K, typename V>
  using multimap_ = set<pair<K,V>, compare<K,V>>;

/**
 * This class manages the core functionalities of the splitter.
 */
class splitter {

 private:

    /**
     * This is the max. size of the splits list.
     */
    static uint64_t t;

    /**
     * This is the min. coverage threshold for k-mers.
     */
    static uint64_t quality;

    /**
     * This is [Q] hash tables mapping k-mers to their colors/splits.
     */
    static vector<hash_map<kmer_t, color_t>> kmer_table;

    /**
     * This is [1] hash table mapping colors/splits to their weights.
     */
    static hash_map<color_t, uint32_t[2]> color_table;

    /**
     * This is [P] hash sets used to filter k-mers for coverage (q > 1).
     */
    static vector<hash_set<kmer_t>> quality_set;

    /**
     * This is [P] hash maps used to filter k-mers for coverage (q > 2).
     */
    static vector<hash_map<kmer_t, uint64_t>> quality_map;

    /**
     * This is [Q] queues used to synchronize k-mers from multiple threads.
     */
    static vector<queue<kmer_t, size1N_t>> thread_queue;

    /**
     * This is the number of k-mers to retrieve from the queue.
     */
    static uint64_t buffer;

 public:

    /**
     * This is a list collecting all the splits ordered by weight.
     */
    static multimap_<double, color_t> splits;

    /**
     * This function initializes the split list size, coverage threshold, and others.
     *
     * @param top_size list size
     * @param quality coverage threshold
     * @param reverse merge complements
     * @param P number of file reading threads
     * @param Q number of queue hashing threads
     */
    static void init(const uint64_t& top_size, const uint64_t& quality, const bool& reverse, const uint64_t& P, const uint64_t& Q);

    /**
     * This function extracts k-mers from a sequence and adds them to the hash table.
     *
     * @param T thread id [P]
     * @param str marker sequence
     * @param color color flag
     */
    static void add_kmers(const uint64_t& T, const string& str, const size1N_t& color);

    /**
     * This function iterates over the hash tables and calculates the split weights.
     *
     * @param mean weight function
     * @param verbose print progress
     */
    static void calc_weights(const function<double(const uint32_t&, const uint32_t&)>& mean, const bool& verbose);

    /**
     * This function filters a greedy maximum weight tree compatible subset.
     *
     * @param verbose print progress
     */
    static void filter_strict(const bool& verbose);

    /**
     * This function filters a greedy maximum weight weakly compatible subset.
     *
     * @param verbose print progress
     */
    static void filter_weakly(const bool& verbose);

    /**
     * This function filters a greedy maximum weight n-tree compatible subset.
     *
     * @param n number of trees
     * @param verbose print progress
     */
    static void filter_n_tree(const uint64_t& n, const bool& verbose);

    /**
     * This function transfers k-mers & colors from the queue to the k-mer table.
     *
     * @param T thread id [Q]
     */
    static void merge_thread(const uint64_t& T);

    /**
     * This function clears a quality filter after full procession of a color.
     *
     * @param T thread id [P]
     */
    static void clear_filter(const uint64_t& T);

    /**
     * This function erases the quality filters and updates the distributor.
     */
    static void erase_filters();

 protected:

    /**
     * This is a collection of (weakly) compatible splits trees.
     */
    static vector<vector<color_t>> forest;

    /**
     * This function reverse complements a k-mer and applies a gap pattern.
     *
     * @param kmer marker sequence
     */
    static function<void(kmer_t&)> process_kmer;

    /**
     * This function qualifies a k-mer and places it into the hash table.
     *
     * @param T thread id [P]
     * @param kmer marker sequence
     * @param color color flag
     */
    static function<void(const uint64_t&, const kmer_t&, const size1N_t&)> emplace_kmer;

    /**
     * This function tests if a split is compatible with an existing set of splits.
     *
     * @param color new split
     * @param color_set set of splits
     * @return true, if compatible
     */
    static bool test_strict(const color_t& color, const vector<color_t>& color_set);

    /**
     * This function tests if a split is weakly compatible with an existing set of splits.
     *
     * @param color new split
     * @param color_set set of splits
     * @return true, if weakly compatible
     */
    static bool test_weakly(const color_t& color, const vector<color_t>& color_set);

};
