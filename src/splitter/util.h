#include <iostream>
#include "../index.h"
using namespace std;

/**
 * This class can display the help page of the program.
 */
class help {

public:

    /**
     * This function prints a minimal version of the help page.
     */
    static void print_usage();

    /**
     * This function prints an extended version of the help page.
     */
    static void print_extended_usage();

};
