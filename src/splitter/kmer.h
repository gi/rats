#include <iostream>
using namespace std;

#ifndef maxL     // max. tuple length defined
#define maxL 1   // as preprocessor directive
#endif

class kmer_t {
 private:
    int64_t tuple[maxL]{};
    friend struct std::hash<kmer_t>;

 public:
    constexpr kmer_t() noexcept {
    };
    constexpr kmer_t(const kmer_t& other) noexcept {
        for (size_t i = 0; i != maxL; ++i)
            tuple[i] = other.tuple[i];
    }
    constexpr kmer_t& operator=(const kmer_t& other) noexcept {
        for (size_t i = 0; i != maxL; ++i)
            tuple[i] = other.tuple[i];
        return *this;
    }

    constexpr int64_t& operator[](const size_t& pos) noexcept {
        return tuple[pos];
    }
    constexpr bool operator==(const kmer_t& other) const noexcept {
        for (size_t i = maxL-1; i != 0; --i)
            if (tuple[i] != other.tuple[i]) return false;
        return tuple[0] == other.tuple[0];
    }
    constexpr bool operator!=(const kmer_t& other) const noexcept {
        for (size_t i = maxL-1; i != 0; --i)
            if (tuple[i] != other.tuple[i]) return true;
        return tuple[0] != other.tuple[0];
    }

    constexpr size_t operator%(const size_t& value) const noexcept {
        int64_t hash = tuple[0];
        for (size_t i = 1; i != maxL; ++i)
            hash ^= tuple[i];
        return hash % value;
    }
};
template<> struct std::hash<kmer_t> {
    constexpr size_t operator()(const kmer_t& obj) const noexcept {
        int64_t hash = obj.tuple[0];
        for (size_t i = 1; i != maxL; ++i)
            hash ^= obj.tuple[i];
        return hash;
    }
};

/**
 * This class contains functions for working with marker tuples.
 */
class kmer {

 private:

    /**
     * This is a mask to erase certain markers in a gapped tuple pattern.
     */
    static bool mask[maxL];

 public:

    /**
     * This is the length of a tuple (including gap positions).
     */
    static size_t k;

    /**
     * This value indicates if a gap pattern was specified.
     */
    static bool gapped;

    /**
     * This function initializes the tuple length and masks.
     *
     * @param length tuple length
     * @param pattern gapped tuple pattern
     */
    static void init(const size_t& length, const string& pattern);

    /**
     * This function shifts a tuple appending a new marker to the right.
     *
     * @param tuple current tuple
     * @param marker next marker
     */
    static void shift(kmer_t& tuple, const int64_t& marker);

    /**
     * This function unshifts a tuple returning the marker on the right.
     *
     * @param tuple current tuple
     * @param chr right marker
     */
    static void unshift(kmer_t& tuple, int64_t& marker);

    /**
     * This function constructs the reverse complement of a given tuple.
     *
     * @param tuple current tuple
     */
    static void reverse_complement(kmer_t& tuple);

    /**
     * This function constructs the r.c. representative of a given tuple.
     *
     * @param tuple current tuple
     * @return 1 if inverted, 0 otherwise
     */
    static bool reverse_represent(kmer_t& tuple);

    /**
     * This function applies a gap pattern to the tuple.
     *
     * @param tuple current tuple
     */
    static void extract_pattern(kmer_t& tuple);

 protected:

};
