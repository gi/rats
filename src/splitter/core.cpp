#include "core.h"
#include "../ansi.h"

/**
 * This class manages the core functionalities of the splitter.
 */
uint64_t splitter::t;         // max. size of the splits list
uint64_t splitter::quality;   // min. coverage threshold for k-mers
uint64_t splitter::buffer;    // number of k-mers to retrieve from the queue

vector<hash_map<kmer_t, color_t>>  splitter::kmer_table;    // [Q] hash tables mapping k-mers to their colors/splits
hash_map<color_t, uint32_t[2]>     splitter::color_table;   // [1] hash table mapping colors/splits to their weights
vector<hash_set<kmer_t>>           splitter::quality_set;   // [P] hash sets used to filter k-mers for coverage (q > 1)
vector<hash_map<kmer_t, uint64_t>> splitter::quality_map;   // [P] hash maps used to filter k-mers for coverage (q > 2)
vector<queue<kmer_t, size1N_t>>    splitter::thread_queue;  // [Q] queues used to synchronize k-mers from multiple threads

multimap_<double, color_t> splitter::splits;   // list collecting all the splits ordered by weight
vector<vector<color_t>>    splitter::forest;   // collection of (weakly) compatible splits trees

function<void(kmer_t&)>                                         splitter::process_kmer;   // reverse complement a k-mer and apply a gap pattern
function<void(const uint64_t&, const kmer_t&, const size1N_t&)> splitter::emplace_kmer;   // qualify a k-mer and place it into the hash table

/**
 * This function initializes the split list size, coverage threshold, and others.
 *
 * @param top_size list size
 * @param quality coverage threshold
 * @param reverse merge complements
 * @param P number of file reading threads
 * @param Q number of queue hashing threads
 */
void splitter::init(const uint64_t& top_size, const uint64_t& quality, const bool& reverse, const uint64_t& P, const uint64_t& Q) {
    t = top_size; splitter::quality = quality;

    if (!kmer::gapped && !reverse)
        process_kmer = [&] (kmer_t& kmer)
          { };
    if (!kmer::gapped && reverse)
        process_kmer = [&] (kmer_t& kmer)
          { kmer::reverse_represent(kmer); };
    if (kmer::gapped && !reverse)
        process_kmer = [&] (kmer_t& kmer)
          { kmer::extract_pattern(kmer); };
    if (kmer::gapped && reverse)
        process_kmer = [&] (kmer_t& kmer)
          { kmer::extract_pattern(kmer);
            kmer::reverse_represent(kmer); };

    if (Q == 0) { /* single-CPU: read one file at a time, don't use queues, put into one table directly */
        kmer_table.resize(1);
        switch (quality) {
            case 1:
                case 0: /* no quality check */
                emplace_kmer = [&] (const uint64_t& T, const kmer_t& kmer, const size1N_t& color) {
                    kmer_table[0][kmer].set(color);
                };  break;
            case 2:
                quality_set.resize(1);
                emplace_kmer = [&] (const uint64_t& T, const kmer_t& kmer, const size1N_t& color) {
                    if (quality_set[0].find(kmer) == quality_set[0].end()) {
                        quality_set[0].emplace(kmer);
                    } else {
                        quality_set[0].erase(kmer);
                        kmer_table[0][kmer].set(color);
                    }
                };  break;
            default:
                quality_map.resize(1);
                emplace_kmer = [&] (const uint64_t& T, const kmer_t& kmer, const size1N_t& color) {
                    if (quality_map[0][kmer] < quality-1) {
                        quality_map[0][kmer]++;
                    } else {
                        quality_map[0].erase(kmer);
                        kmer_table[0][kmer].set(color);
                    }
                };  break;
        }
    } else { /* multi-CPU: read multiple files in parallel, distribute to multiple queues & tables */
        splitter::buffer = max<uint64_t>(P * (1048576 / sizeof(pair<int64_t[maxL], size1N_t>)) / Q, 4);
        for (uint64_t i = 0; i < Q; ++i) thread_queue.emplace_back(splitter::buffer);
        splitter::buffer = max<uint64_t>(P * (1024 / sizeof(pair<int64_t[maxL], size1N_t>)) / Q, 4);

        kmer_table.resize(Q);
        switch (quality) {
            case 1:
                case 0: /* no quality check */
                emplace_kmer = [&] (const uint64_t& T, const kmer_t& kmer, const size1N_t& color) {
                    const pair<const kmer_t&, const size1N_t&> item(kmer, color);
                    while (!thread_queue[kmer % Q].try_push(item));  // wait for enough space
                };  break;
            case 2:
                quality_set.resize(P);
                emplace_kmer = [&] (const uint64_t& T, const kmer_t& kmer, const size1N_t& color) {
                    if (quality_set[T].find(kmer) == quality_set[T].end()) {
                        quality_set[T].emplace(kmer);
                    } else {
                        quality_set[T].erase(kmer);
                        const pair<const kmer_t&, const size1N_t&> item(kmer, color);
                        while (!thread_queue[kmer % Q].try_push(item));  // wait for enough space
                    }
                };  break;
            default:
                quality_map.resize(P);
                emplace_kmer = [&] (const uint64_t& T, const kmer_t& kmer, const size1N_t& color) {
                    if (quality_map[T][kmer] < quality-1) {
                        quality_map[T][kmer]++;
                    } else {
                        quality_map[T].erase(kmer);
                        const pair<const kmer_t&, const size1N_t&> item(kmer, color);
                        while (!thread_queue[kmer % Q].try_push(item));  // wait for enough space
                    }
                };  break;
        }
    }
}

/**
 * This function extracts k-mers from a sequence and adds them to the hash table.
 *
 * @param T thread id [P]
 * @param str marker sequence
 * @param color color flag
 */
void splitter::add_kmers(const uint64_t& T, const string& str, const size1N_t& color) {
    kmer_t kmer; kmer_t rcmer;

    istringstream stream(str); string marker;
    for (size_t i = 0; i < kmer::k; ++i) {
        if (getline(stream, marker, ' ')) {
            kmer::shift(kmer, stoll(marker));
        } else return;    // not enough characters
    }
    rcmer = kmer;
    process_kmer(rcmer);    // invert the k-mer & apply gap pattern, if necessary
    emplace_kmer(T, rcmer, color);    // update the k-mer with the current color

    while (getline(stream, marker, ' ')) {
        kmer::shift(kmer, stoll(marker));
        rcmer = kmer;
        process_kmer(rcmer);    // invert the k-mer & apply gap pattern, if necessary
        emplace_kmer(T, rcmer, color);    // update the k-mer with the current color
    }
}

/**
 * This function iterates over the hash tables and calculates the split weights.
 *
 * @param mean weight function
 * @param verbose print progress
 */
void splitter::calc_weights(const function<double(const uint32_t&, const uint32_t&)>& mean, const bool& verbose) {
    double min_value = numeric_limits<double>::min();    // current min. weight in the splits list
    uint64_t cur = 0, max = 0, prog = 0, next;
    for (auto& _kmer_table : kmer_table)
        max += _kmer_table.size();

    for (auto& _kmer_table : kmer_table) {
        for (auto it = _kmer_table.begin(); it != _kmer_table.end(); ++it) {    // iterate over the k-mer table
            if (verbose) {
                next = 100 * cur / max;
                 if (prog < next)  $log $_ << "Processing splits... " << next << "%" << $;
                prog = next; cur++;
            }
            color_t& color = it.value();    // get the color set for each k-mer
            bool pos = color::represent(color);    // invert the color set, if necessary
            if (color != 0b0u) color_table[color][pos]++;    // update the weight or inverse weight
        }
    }
    for (auto it = color_table.begin(); it != color_table.end(); ++it) {    // iterate over the color table
        const auto& color = it.key();    // get the color set of the split
        const auto& weight = it.value();    // get the weights for each split
        double new_value = mean(weight[0], weight[1]);    // calculate the new mean value

        if (new_value >= min_value) {    // if it is greater than the min. value, add it to the top list
            splits.emplace(new_value, color);    // insert it at the correct position ordered by weight
            if (splits.size() > splitter::t) {      // if the splits list exceeds its limit, erase the last entry
                splits.erase(--splits.end());
                min_value = splits.rbegin()->first;    // update the min. value for the next iteration
            }
        }
    }
}

/**
 * This function filters a greedy maximum weight tree compatible subset.
 *
 * @param verbose print progress
 */
void splitter::filter_strict(const bool& verbose) {
    forest = vector<vector<color_t>>(1);    // create a set for compatible splits
    auto& tree = forest[0];
    auto it = splits.begin();

    uint64_t cur = 0, prog = 0, next;
    uint64_t max = splits.size();
loop:
    while (it != splits.end()) {
        if (verbose) {
            next = 100 * cur / max;
             if (prog < next)  $log $_ << "Filtering splits... " << next << "%" << $;
            prog = next; cur++;
        }
        if (test_strict(it->second, tree)) {
            tree.emplace_back(it->second);
            ++it; goto loop;    // if compatible, add the new split to the set
        }
        it = splits.erase(it);    // otherwise, remove split
    }
}

/**
 * This function filters a greedy maximum weight weakly compatible subset.
 *
 * @param verbose print progress
 */
void splitter::filter_weakly(const bool& verbose) {
    forest = vector<vector<color_t>>(1);    // create a set for compatible splits
    auto& network = forest[0];
    auto it = splits.begin();

    uint64_t cur = 0, prog = 0, next;
    uint64_t max = splits.size();
loop:
    while (it != splits.end()) {
        if (verbose) {
            next = 100 * (cur * sqrt(cur)) / (max * sqrt(max));
             if (prog < next)  $log $_ << "Filtering splits... " << next << "%" << $;
            prog = next; cur++;
        }
        if (test_weakly(it->second, network)) {
            network.emplace_back(it->second);
            ++it; goto loop;    // if compatible, add the new split to the set
        }
        it = splits.erase(it);    // otherwise, remove split
    }
}

/**
 * This function filters a greedy maximum weight n-tree compatible subset.
 *
 * @param n number of trees
 * @param verbose print progress
 */
void splitter::filter_n_tree(const uint64_t& n, const bool& verbose) {
    forest = vector<vector<color_t>>(n);    // create a set for compatible splits
    auto it = splits.begin();

    uint64_t cur = 0, prog = 0, next;
    uint64_t max = splits.size();
loop:
    while (it != splits.end()) {
        if (verbose) {
            next = 100 * cur / max;
             if (prog < next)  $log $_ << "Filtering splits... " << next << "%" << $;
            prog = next; cur++;
        }
        for (auto& tree : forest)
            if (test_strict(it->second, tree)) {
                tree.emplace_back(it->second);
                ++it; goto loop;    // if compatible, add the new split to the set
            }
        it = splits.erase(it);    // otherwise, remove split
    }
}

/**
 * This function tests if a split is compatible with an existing set of splits.
 *
 * @param color new split
 * @param color_set set of splits
 * @return true, if compatible
 */
bool splitter::test_strict(const color_t& color, const vector<color_t>& color_set) {
    for (auto& elem : color_set) {
        if (!color::is_compatible(elem, color)) {
            return false;    // compare to each split in the set
        }
    }
    return true;
}

/**
 * This function tests if a split is weakly compatible with an existing set of splits.
 *
 * @param color new split
 * @param color_set set of splits
 * @return true, if weakly compatible
 */
bool splitter::test_weakly(const color_t& color, const vector<color_t>& color_set) {
    for (auto& elem1 : color_set) {
        if (!color::is_compatible(elem1, color)) {
            for (auto& elem2 : color_set) {
                if (!color::is_weakly_compatible(elem1, elem2, color)) {
                    return false;    // compare to each pair of splits in the set
                }
            }
        }
    }
    return true;
}

/**
 * This function transfers k-mers & colors from the queue to the k-mer table.
 *
 * @param T thread id [Q]
 */
void splitter::merge_thread(const uint64_t& T) {
    pair<kmer_t, size1N_t> items[buffer]; size_t items_size;
    do { items_size = thread_queue[T].try_pop_bulk(items, buffer);
        for (uint64_t i = 0; i != items_size; ++i)
            kmer_table[T][items[i].first].set(items[i].second);
    }  while (items_size != 0);   // repeat until queue is empty
}

/**
 * This function clears a quality filter after full procession of a color.
 *
 * @param T thread id [P]
 */
void splitter::clear_filter(const uint64_t& T) {
    switch (quality) {
        case 1:  case 0: break;
        case 2:  quality_set[T].clear(); break;
        default: quality_map[T].clear(); break;
    }
}

/**
 * This function erases the quality filters after full procession of all colors.
 */
void splitter::erase_filters() {
    switch (quality) {
        case 1:  case 0: break;
        case 2:  quality_set.clear(); break;
        default: quality_map.clear(); break;
    }
}
