#include "index.h"
#include "ansi.h"

/**
 * This class provides an interface for handling different file types.
 */
file::file(const string& path, const type& type, const mode& mode) {
    _path = path; _type = type; _mode = mode; properties.clear();

    if (_path.empty())       $err << "Error: file path not specified"            << _end$$;
    if (_type == type::NONE) $err << "Error: file type not specified: " << _path << _end$$;
    if (_mode == mode::NONE) $err << "Error: file mode not specified: " << _path << _end$$;

    if (_type == type::GRAPH_FILE) {
        if (_mode == mode::WRITE) $err << "Error: file mode not supported: " << _path << _end$$;

        size_t pos; string ext;
         if ((pos = _path.rfind(".gfa.gz"))     != string::npos && (ext = _path.substr(pos)) == ".gfa.gz"
         ||  (pos = _path.rfind(".gfa"))        != string::npos && (ext = _path.substr(pos)) == ".gfa"
         ||  (pos = _path.rfind(".color.bfg"))  != string::npos && (ext = _path.substr(pos)) == ".color.bfg"
         ||  (pos = _path.rfind(".bfg_colors")) != string::npos && (ext = _path.substr(pos)) == ".bfg_colors");
        string prefix = _path.substr(0, pos);

         if (ext != ".gfa" && ifstream(properties["graph_fn"] = prefix + ".gfa.gz").good()
         ||  ext != ".gfa.gz" && ifstream(properties["graph_fn"] = prefix + ".gfa").good());
        else $err << "Error: could not find Bifrost graph file (.gfa.gz)" << _end$$;

         if (ext != ".bfg_colors" && ifstream(properties["color_fn"] = prefix + ".color.bfg").good()
         ||  ext != ".color.bfg" && ifstream(properties["color_fn"] = prefix + ".bfg_colors").good());
        else $err << "Error: could not find Bifrost color file (.color.bfg)" << _end$$;
    }

    if (_type == type::BREAK_FILE) {
        if (_mode == mode::WRITE) $err << "Error: file mode not supported: " << _path << _end$$;

        size_t pos; string ext;
         if ((pos = _path.rfind(".L")) != string::npos && (ext = _path.substr(pos)) == ".L"
         ||  (pos = _path.rfind(".R")) != string::npos && (ext = _path.substr(pos)) == ".R");
        string prefix = _path.substr(0, pos);

         if (ifstream(properties["L"] = prefix + ".L").good()
         &&  ifstream(properties["R"] = prefix + ".R").good());
        else $err << "Error: could not find breakpoint k-mer files (.L/R)" << _end$$;
    }
}

/**
 * This function converts a command line argument to a string.
 *
 * @param argc argument count
 * @param argv argument vector
 * @param i argument index
 * @return string
 */
string util::atos(int& argc, char* argv[], int& i) {
    if (i < argc) {
        return argv[i];
    } else {
        $err << "Error: incomplete argument: " << argv[i-1] << " (expecting a string)" << _end$$;
    }
}

/**
 * This function converts a command line argument to a number.
 *
 * @param argc argument count
 * @param argv argument vector
 * @param i argument index
 * @return number
 */
uint64_t util::aton(int& argc, char* argv[], int& i) {
    if (i < argc) {
        try {
            const auto& n = stoull(argv[i]);
            if (n < 1) {
                $err << "Error: argument: " << argv[i-1] << ' ' << argv[i] << " (expected a positive integer)" << _end$$;
            } else return n;
        } catch (...) {
            $err << "Error: malformed argument: " << argv[i-1] << ' ' << argv[i] << " (expected a number)" << _end$$;
        }
    } else {
        $err << "Error: incomplete argument: " << argv[i-1] << " (expecting a number)" << _end$$;
    }
}

/**
 * This function converts a string argument to a number.
 *
 * @param param parameter
 * @param args argument
 * @return number
 */
uint64_t util::ston(const string& param, const string& args) {
    try {
        const auto& n = stoull(args);
        if (n < 1) {
            $err << "Error: argument: " << param << ' ' << args << " (expected a positive integer)" << _end$$;
        } else return n;
    } catch (...) {
        $err << "Error: malformed argument: " << param << ' ' << args << " (expected a number)" << _end$$;
    }
}

/**
 * This function displays a duration in a human readable format.
 *
 * @param time duration
 * @return formatted string
 */
string util::format_time(const chrono::high_resolution_clock::duration& time) {
    double value = chrono::duration_cast<chrono::milliseconds>(time).count();
    string unit = "ms";
    if (value >= 1000) {
        value /= 1000; unit = "sec";
        if (value >= 200) {
            value /= 60; unit = "min";
            if (value >= 200) {
                value /= 60; unit = "h";
            }
        }
    }
    string number = to_string(value);
    return number.substr(0, number.find('.')+2) + ' ' + unit;
}
