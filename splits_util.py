from math import sqrt
def canonize_splits(splits,universe):
    canon = []
    tiebreaker = min(universe)
    for w,s in splits:
        if len(s) > len(universe)/2 or (len(s)==len(universe)/2 and not tiebreaker in s):
            s = sorted(list(universe.difference(set(s))))
        canon.append((w,s))
    return canon

def read_sans_splitfile(fln):
    universe = set()
    splits = []
    with open(fln) as f:
        for line in f:
            entries = line.strip().split('\t')
            weight = float(entries[0])
            split_side_a = entries[1::]
            universe.update(split_side_a)
            splits.append((weight, split_side_a))
    return canonize_splits(splits,universe)



def arith_mean(a,b):
    return (a+b)/2

def geom_mean(a,b):
    return sqrt(a*b)


def ratio(a,b):
    return a/(a+b)

