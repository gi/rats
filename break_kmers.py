#!/usr/bin/env python3

import sys, re

if len(sys.argv) <= 1 or sys.argv[1] == "-h" or sys.argv[1] == "--help":
    print("")
    print(" Extract breakpoint k-mers from a list of FASTA sequences")
    print(" (i.e. k-mers at chromosome start/ends, k-mers around Ns)")
    print("")
    print("   -i, --input   \t Input file: list of sequence files, one per line")
    print("   -o, --output  \t Output file: list of left/right breakpoint k-mers")
    print("")
    print("   -k, --kmer    \t Length of k-mers (default: 31)")
    print("")
    print("   -v, --verbose \t Print information messages during execution")
    print("   -h, --help    \t Display the current help page and quit")
    print("")
    sys.exit(0)

ifile = ""    # Input file: list of sequence files, one per line
ofile = ""    # Output file: list of left/right breakpoint k-mers
kmer = 31          # Length of k-mers (default: 31)
verbose = False    # Print some information messages during execution
VERBOSE = False    # Print more information messages during execution

i = 1
while i < len(sys.argv):
    if sys.argv[i] == "-i" or sys.argv[i] == "--input":
        ifile = sys.argv[i+1]; i += 1    # Input file
    elif sys.argv[i] == "-o" or sys.argv[i] == "--output":
        ofile = sys.argv[i+1]; i += 1    # Output file
    elif sys.argv[i] == "-k" or sys.argv[i] == "--kmer":
        kmer = int(sys.argv[i+1]); i += 1
    elif sys.argv[i] == "-v" or sys.argv[i] == "--verbose":
        verbose = True    # Print information messages
    elif sys.argv[i] == "-V":    ## hidden option ##
        verbose = True    # Print some information messages
        VERBOSE = True    # Print more information messages
    elif sys.argv[i] != "-h" and sys.argv[i] != "--help":
        print(f"Error: unknown argument: {sys.argv[i]}", file=sys.stderr)
        print( "       type --help to see the full list of parameters", file=sys.stderr)
        sys.exit(1)
    i += 1

if not ifile.strip():
    print("Error: missing input argument: --input <file_name>", file=sys.stderr)
    sys.exit(1)
if not ofile.strip():
    print("Error: missing output argument: --output <file_name>", file=sys.stderr)
    sys.exit(1)

# parse the list of input sequence files
file_names = []
ifile = open(ifile, 'r')
for line in ifile:
    file_names.append(line.strip())
ifile.close()

left_breakpoints = set()
right_breakpoints = set()
if verbose:
    print("Reading input files...")

for i in range(len(file_names)):
    ifastx = open(file_names[i], 'r')
    if verbose:
        print(f" + {file_names[i]} ({i+1}/{len(file_names)})")
    
    sequence = []
    for line in ifastx:
        if line.strip():
            if line[0] == '>' or line[0] == '@':
                if VERBOSE:
                    print(f"   {line.strip()[0:75]}…")
        break
    
    for line in ifastx:
        if line.strip():
            if line[0] == '>' or line[0] == '@':
                sequence = re.split("[^ACGT]+", "".join(sequence))
                for unitig in sequence:
                    if len(unitig) >= kmer:
                        left_breakpoints.add(unitig[0:kmer])
                        right_breakpoints.add(unitig[-kmer:])
                sequence.clear()
                if VERBOSE:
                    print(f"   {line.strip()[0:75]}…")
            else:
                sequence.append(line.strip().upper())
    
    sequence = re.split("[^ACGT]+", "".join(sequence))
    for unitig in sequence:
        if len(unitig) >= kmer:
            left_breakpoints.add(unitig[0:kmer])
            right_breakpoints.add(unitig[-kmer:])
    sequence.clear()
    ifastx.close()

ofileL = open(f"{ofile}.L", 'w')
for kmer in left_breakpoints:
    ofileL.write(f"{kmer}\n")
ofileL.close()

ofileR = open(f"{ofile}.R", 'w')
for kmer in right_breakpoints:
    ofileR.write(f"{kmer}\n")
ofileR.close()
