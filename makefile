# MAX. K-MER LENGTH, NUMBER OF FILES, LINK BIFROST LIBRARY
CC = g++ -O3 -flto=auto -march=native -DmaxK=31 -DmaxL=2 -DmaxN=11
LBFZ = -lbifrost -lpthread -lz
LX = -lpthread

main: obj/ obj/speller/main.o obj/splitter/main.o
	$(CC) obj/speller/main.o obj/speller/core.o obj/speller/util.o obj/speller/kmer.o obj/color.o obj/index.o -o Speller $(LBFZ)
	$(CC) obj/splitter/main.o obj/splitter/core.o obj/splitter/util.o obj/splitter/kmer.o obj/color.o obj/index.o -o Splitter $(LX)

obj/speller/main.o: src/speller/main.cpp src/speller/main.h obj/speller/core.o obj/speller/util.o
	$(CC) -c src/speller/main.cpp -o obj/speller/main.o
obj/splitter/main.o: src/splitter/main.cpp src/splitter/main.h obj/splitter/core.o obj/splitter/util.o
	$(CC) -c src/splitter/main.cpp -o obj/splitter/main.o

obj/speller/core.o: src/speller/core.cpp src/speller/core.h obj/speller/kmer.o obj/color.o
	$(CC) -c src/speller/core.cpp -o obj/speller/core.o
obj/splitter/core.o: src/splitter/core.cpp src/splitter/core.h obj/splitter/kmer.o obj/color.o
	$(CC) -c src/splitter/core.cpp -o obj/splitter/core.o

obj/speller/util.o: src/speller/util.cpp src/speller/util.h obj/index.o
	$(CC) -c src/speller/util.cpp -o obj/speller/util.o
obj/splitter/util.o: src/splitter/util.cpp src/splitter/util.h obj/index.o
	$(CC) -c src/splitter/util.cpp -o obj/splitter/util.o

obj/speller/kmer.o: src/speller/kmer.cpp src/speller/kmer.h src/byte.h
	$(CC) -c src/speller/kmer.cpp -o obj/speller/kmer.o
obj/splitter/kmer.o: src/splitter/kmer.cpp src/splitter/kmer.h
	$(CC) -c src/splitter/kmer.cpp -o obj/splitter/kmer.o

obj/color.o: src/color.cpp src/color.h src/byte.h
	$(CC) -c src/color.cpp -o obj/color.o

obj/index.o: src/index.cpp src/index.h
	$(CC) -c src/index.cpp -o obj/index.o

obj/: makefile src/ansi.h src/tsl/*.h src/mcl/*.h
	@rm -rf obj/ && mkdir -p obj/speller/ obj/splitter/
