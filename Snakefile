configfile: 'config.yaml'
import datetime
from math import log10,ceil,floor
import sys 
import os
import re

def read_taxa():
  d = {}
  rd = {}
  with open(config['taxa_names']) as tn:
    for line in tn:
      entries = line.strip().split()
      d[entries[0]]=entries[1]
      rd[entries[1]]=entries[0]
  return d,rd

TAXON_NAME,GENOME_FILE = read_taxa()

GENOMES = sorted(list(GENOME_FILE.keys()))


N_GENOMES = len(GENOMES)
N_DIGITS = ceil(log10(N_GENOMES+1))
IDX_FMT_STR = '%0'+str(N_DIGITS)+'d'
idx_to_id = lambda i : IDX_FMT_STR%i

WF_HOME = str(os.getcwd())
DBG_K = config["k"] if "k" in config else 31
ID_OF_GENOME = dict([(x,idx_to_id(i)) for i,x in enumerate(GENOMES,start=1)])
GENOME_OF_ID = dict([(v,k) for k,v in ID_OF_GENOME.items()])
QUORUM_PERCENT = config["quorum_percent"] if "quorum_percent" in config else 0.9
CORE_DELTA = config["core_delta"] if "core_delta" in config else 2*DBG_K
TAXON_PAIRS = ['{}_{}'.format(a,b) for a in GENOMES for b in GENOMES if a < b]
TODO_ANDREAS = 'echo "@Andreas: TODO"'
UNITIG_MIN_LEN = config['unitig_min_l'] if 'unitig_min_l' in config else 2*DBG_K
UNITIG_MIN_COLORS = config['unitig_min_colors'] if 'unitig_min_colors' in config else 0
RUN_DIR = config['rundirectory'] if 'rundirectory' in config else '-'.join(GENOMES)
DING_PATH = config['ding_path']
BIFROST = config['bifrost_executable']
CORER = config['corer_executable']
SPLITSTREE = config['splitstree_cmd_executable']
MERGE_BUBBLES = config['merge_bubbles'] if 'merge_bubbles' in config else 0
SPELL_SOFT = config['spell_soft'] if 'spell_soft' in config else 'n'
HEURISTIC = config['heuristic_executable']
A = config["a"] if "a" in config else "n"
GUROBI_TIMELIM = config['gurobi_timelim'] if 'gurobi_timelim' in config else 1000*365*24*60*60 #set to 1000 years if user does not specify
GUROBI_THREADS = config['gurobi_threads'] if 'gurobi_threads' in config else 1
MERGEHOW = config["mergehow"] if "mergehow" in config else "r"
QUORUM = floor(QUORUM_PERCENT*N_GENOMES) if not 'quorum' in config else config['quorum']

rule rename_colorfile:
   input:
     '{prefix}/{basename}.color.bfg'
   output:
     '{prefix}/{basename}.bfg_colors'
   shell:
     'cp {input} {output}'


rule taxon_list:
  output:
    '{run}/taxon_list.txt'
  run:
    with open(output[0],'w') as f:
      for g in GENOMES:
        print(GENOME_FILE[g],file=f)

rule build_dbg:
  input:
    fasta=expand('%s/{gf}'%RUN_DIR,gf=[GENOME_FILE[g] for g in GENOMES]),
    taxon_list='%s/taxon_list.txt'%RUN_DIR
  output:
    graph='%s/dbg_k_{k}/dbg.gfa.gz'%RUN_DIR,
    colors='%s/dbg_k_{k}/dbg.color.bfg'%RUN_DIR,
    bfi='%s/dbg_k_{k}/dbg.bfi'%RUN_DIR,
  log:
    one='%s/dbg_k_{k}/bifrost.log'%RUN_DIR,
    two='%s/dbg_k_{k}/bifrost2.log'%RUN_DIR
  threads:
    workflow.cores
  shell:
    'cd %s; %s build -c -v -k {wildcards.k} -t {threads} -s taxon_list.txt -o dbg_k_{wildcards.k}/dbg > {log.one} 2> {log.two}'%(RUN_DIR,BIFROST)

rule compile_speller:
  output:
    temp('Speller')
  shell:
    'make'

rule get_breaking_kmers:
  input:
    fasta=expand('{{run}}/{gf}',gf=[GENOME_FILE[g] for g in GENOMES]),
    taxlist='{run}/taxon_list.txt'
  output:
    '{run}/dbg_k_{k}/breaks.L',
    '{run}/dbg_k_{k}/breaks.R'
  shell:
    'cd {wildcards.run}; %s/break_kmers.py -i taxon_list.txt -o {wildcards.run}/dbg_k_{wildcards.k}/breaks -k {wildcards.k}'%WF_HOME

rule uncompress_dbg:
   input:
     '{run}/dbg_k_{k}/dbg.gfa.gz'
   output:
     '{run}/dbg_k_{k}/dbg.gfa'
   shell:
     'gunzip {input}'

rule dbg_to_core:
   input:
     ugraph='{run}/dbg_k_{k}/dbg.gfa',
     #cgraph='{run}/dbg_k_{k}/dbg.gfa.gz',
     bfi='{run}/dbg_k_{k}/dbg.bfi',
     colors='{run}/dbg_k_{k}/dbg.bfg_colors'
   output:
     ugraph='{run}/core_k_{k}_q_{q}_d_{d}/core.gfa',
     colors='{run}/core_k_{k}_q_{q}_d_{d}/core.color.bfg',
     bfi='{run}/core_k_{k}_q_{q}_d_{d}/core.bfi'
   log:
     one='{run}/core_k_{k}_q_{q}_d_{d}/core1.log',
     two='{run}/core_k_{k}_q_{q}_d_{d}/core2.log'
   threads:
     workflow.cores
   shell:
      'cd {wildcards.run} ; %s -i dbg_k_{wildcards.k}/dbg -o core_k_{wildcards.k}_q_{wildcards.q}_d_{wildcards.d}/core -q {wildcards.q} -d {wildcards.d} -t {threads} > core_k_{wildcards.k}_q_{wildcards.q}_d_{wildcards.d}/core1.log  2> core_k_{wildcards.k}_q_{wildcards.q}_d_{wildcards.d}/core2.log '%CORER

rule core_to_spelled:
  input:
    graph='{run}/core_k_{k}_q_{q}_d_{d}/core.gfa',
    colors='{run}/core_k_{k}_q_{q}_d_{d}/core.color.bfg',
    tlist='{run}/taxon_list.txt',
    speller='Speller',
    breaks='{run}/dbg_k_{k}/breaks.L'
  log:
    one='{run}/spelled/k_{k}_q_{q}_d_{d}_minlen_{ml}_mincol_{mc}_bubble_{bb}_soft_{s}_a_{a}_r_{r}/speller1.log',
    two='{run}/spelled/k_{k}_q_{q}_d_{d}_minlen_{ml}_mincol_{mc}_bubble_{bb}_soft_{s}_a_{a}_r_{r}/speller2.log'
  output:
    expand('{{run}}/spelled/k_{{k}}_q_{{q}}_d_{{d}}_minlen_{{ml}}_mincol_{{mc}}_bubble_{{bb}}_soft_{{s}}_a_{{a}}_r_{{r}}/spelled.{num}',num=ID_OF_GENOME.values())
  threads:
    min(workflow.cores,len(GENOMES))
  run:
    #TODO: this is a risky move
    wildcards.coreparams = "k_%s_q_%s_d_%s"%(wildcards.k,wildcards.q,wildcards.d)
    wildcards.spellerparams = "minlen_%s_mincol_%s_bubble_%s_soft_%s_a_%s_r_%s"%(wildcards.ml,wildcards.mc,wildcards.bb,wildcards.s,wildcards.a,wildcards.r)
    mycall=('cd {wildcards.run};  %s/Speller -i taxon_list.txt -g core_k_{wildcards.k}_q_{wildcards.q}_d_{wildcards.d}/core'%WF_HOME 
    +' -u spelled/{wildcards.coreparams}_{wildcards.spellerparams}/spelled'
    #+' -o spelled/{wildcards.coreparams}_{wildcards.spellerparams}/markerseq'
    +' -k {wildcards.k} -t {threads} '+(' -s ' if wildcards.s=='y' else '')
    + (' -m %s'%wildcards.bb if int(wildcards.bb)>0 else '')
    + (' -c {wildcards.mc}' if int(wildcards.mc)>0 else '')
    + (' -S ' if wildcards.a=='y' else '')
    + (' -C %s '%wildcards.q if wildcards.a=='y' else '')
    + (' -L {wildcards.ml} ' if wildcards.a=='y' else ' -l {wildcards.ml} ' )
    + ' -b dbg_k_{wildcards.k}/breaks'
    + (' -M ' if wildcards.a=='y' else '')
    + (' -%s '%wildcards.r if wildcards.r in ['r','R'] else '')
    +' -v > spelled/{wildcards.coreparams}_{wildcards.spellerparams}/speller1.log'
    +' 2> spelled/{wildcards.coreparams}_{wildcards.spellerparams}/speller2.log')
    print(mycall)
    shell(mycall)

rule marker_sets:
   input:
     '{run}/unimog/{params}/{genome}.txt'
   output:
     '{run}/unimog/{params}/{genome}.uq'
   shell:
     'grep -v ">" {input} | tr -d "-" | tr " " "\n" | grep -v "|" | sort | uniq > {output}'

#TODO: Add recognition of linear and circular fragments
rule spelled_to_unimog:
  input:
    expand('{{run}}/spelled/{{params}}/spelled.{num}',num=GENOME_OF_ID.keys())
  output:
    '{run}/unimog/{params}/{genome}.txt'
  run:
    with open(output[0],"w") as f:
      infile = '{run}/spelled/{params}/spelled.{num}'.format(run=wildcards.run
                                      ,params=wildcards.params
                                      ,num=ID_OF_GENOME[wildcards.genome])
      with open(infile) as ff:
        for line in ff:
          if line.startswith(">"):
            tn = TAXON_NAME[line.strip()[1::].strip()]
            if tn != wildcards.genome:
                print("Wrong taxon name! Expected {}, but got {}".format(wildcards.genome,tn))  
                sys.exit(1)
            print(">"+tn, file=f)
          else:
            stripped = re.sub(r"\b-0\b","",re.sub(r"\b0\b","",line.strip())).strip()
	    if len(stripped) > 0:
               print(stripped+" |",file=f)

rule all_unimog:
  input:
    expand('{run}/unimog/k_{k}_q_{q}_d_{d}_minlen_{ml}_mincol_{mc}_bubble_{bb}_soft_{s}_a_{a}_r_{r}/{genome}.txt',
            run=RUN_DIR,k=DBG_K,q=QUORUM,d=CORE_DELTA,ml=UNITIG_MIN_LEN,mc=UNITIG_MIN_COLORS,bb=MERGE_BUBBLES,s=SPELL_SOFT,genome=GENOMES,a=A,r=MERGEHOW)

rule all_unique:
  input:
    expand('{run}/unimog/k_{k}_q_{q}_d_{d}_minlen_{ml}_mincol_{mc}_bubble_{bb}_soft_{s}_a_{a}_r_{r}/{genome}.uq',
            run=RUN_DIR,k=DBG_K,q=QUORUM,d=CORE_DELTA,ml=UNITIG_MIN_LEN,mc=UNITIG_MIN_COLORS,bb=MERGE_BUBBLES,s=SPELL_SOFT,genome=GENOMES,a=A,r=MERGEHOW)



rule unimog_to_pair:
  input:
    a='{run}/unimog/{params}/{genomea}.txt',
    b='{run}/unimog/{params}/{genomeb}.txt'
  output:
    temp('{run}/pairs/{params}/{genomea}_{genomeb}.txt')
  shell:
    'cat {input.a} | sed "s/>{wildcards.genomea}/>A/" > {output} ; cat {input.b} | sed "s/>{wildcards.genomeb}/>B/" >> {output}' 


rule pair_to_ilp:
  input:
    '{run}/pairs/{params}/{pair}.txt'
  output:
    '{run}/ilp/{params}/{pair}.lp'
  log:
    '{run}/ilp/{params}/{pair}.log'
  threads:
    4
  shell:
    '%s/dingII.py {input} -p A B --writeilp {output} 2> {log}'%DING_PATH

rule solve_ilp:
  input:
    '{run}/ilp/{params}/{pair}.lp'
  output:
    '{run}/sol/{params}/{pair}.sol'
  log:
    one='{run}/sol/{params}/{pair}.log',
    two='{run}/sol/{params}/{pair}.log2'
  threads:
    GUROBI_THREADS
  shell:
    './solve_gurobi.py -t {threads} --timelim %i {input} {output} > {log.one} 2> {log.two}'%(GUROBI_TIMELIM)

rule get_dist_and_matching:
    input:
        sol='{run}/sol/{params}/{pair}.sol',
        uni='{run}/pairs/{params}/{pair}.txt'
    output:
        match='{run}/match/{params}/{pair}.txt',
        dist='{run}/dist/{params}/{pair}.dist'
    shell:
        '%s/dingII_parsesol.py --solgur {input.sol} --matching {output.match} {input.uni} > {output.dist}'%DING_PATH

rule raw_dist:
  input:
    '{run}/dist/{params}/{pair}.dist'
  output:
    '{run}/dist/{params}/{pair}.txt'
  shell:
    'cut -d "=" -f 2 {input} > {output}'

'''
rule heuristic_dist:
   input:
      '{run}/pairs/{params}/{pair}.txt'
   output:
      '{run}/dist/{params}/{pair}.txt'
   log:
      '{run}/dist/{params}/{pair}.log'
   threads:
      1
   shell:
      '%s --unimog {input} > {output} 2> {log}'%HEURISTIC
'''

rule gnm_size:
   input:
      '{run}/unimog/{params}/{taxon}.txt'
   output:
      '{run}/unimog/{params}/{taxon}.size'
   shell:
      'grep -v ">" {input} | tr -d "|" | wc -w > {output}'

rule dist_phylip:
    input:
        expand('{{run}}/dist/{{parameters}}/{pair}.txt',pair=TAXON_PAIRS)
    output:
        '{run}/stats/{parameters}/distances.dist'
    shell:
        'python3 to_matrix.py --outfmt phylip --files {input} > {output}'

rule normalize_distance:
   input:
      sizes=expand('{{run}}/unimog/{{parameters}}/{taxon}.size',taxon=GENOMES),
      distmat='{run}/{stats}/{parameters}/distances.dist'
   output:
      '{run}/{stats}/{parameters}/normed_distances.dist'
   shell:
     'python3 norm_distances.py --sizes {input.sizes} --distmat {input.distmat} > {output}'

rule all_dist:
  input:
    expand('{run}/stats/k_{k}_q_{q}_d_{d}_minlen_{ml}_mincol_{mc}_bubble_{bb}_soft_{s}_a_{a}_r_{r}/distances.dist',k=DBG_K,q=QUORUM,d=CORE_DELTA,ml=UNITIG_MIN_LEN,mc=UNITIG_MIN_COLORS,bb=MERGE_BUBBLES,s=SPELL_SOFT,run=RUN_DIR,a=A,r=MERGEHOW)

rule makesplitstreecommands:
    output:
        temp("{run}/splitstreecommands/basic.txt")
    shell:
        'echo "LOAD FILE={wildcards.run}/distances.dist" > {output};'
        +'echo UPDATE >> {output};'
        +'echo "SAVE FILE={wildcards.run}/splitstreecommands/basic.nex" >> {output};'

rule run_splitstree_basic:
    input:
      cmd="{run}/splitstreecommands/basic.txt",
      dist="{run}/distances.dist"
    output:
      "{run}/splitstreecommands/basic.nex"
    shell:
      '%s --commandLineMode -c {input.cmd} > {log}'%SPLITSTREE
